import React, {Component} from 'react'
import {View, Text} from 'react-native'

export class ActivityRow extends Component{
    constructor(props){
        super(props)
        var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
        var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
        Date.prototype.getMonthName = function() {
            return months[ this.getMonth() ];
        };
        Date.prototype.getDayName = function() {
            return days[ this.getDay() ];
        };
    }

    render(){
        return(<View style={{flexDirection: 'row', ...this.props.style}}>
                {this.props.activities.map((item) => <View style={{
                                                            backgroundColor: '#24242410', 
                                                            position: 'absolute', 
                                                            top: this.props.activityMargin, 
                                                            left: (this.props.rowsWidth*item.time.start.getUTCHours())  + (item.time.start.getUTCMinutes()*this.props.rowsWidth/60) + this.props.activityMargin, 
                                                            width: (item.time.end.getUTCHours() - item.time.start.getUTCHours())*this.props.rowsWidth + (item.time.end.getUTCMinutes() - item.time.start.getUTCMinutes())*this.props.rowsWidth/60 - 2*this.props.activityMargin, 
                                                            height: this.props.rowsHeight - 2*this.props.activityMargin - 4, 
                                                            borderRadius:5, 
                                                            borderColor: '#b28bec', 
                                                            borderWidth: 1,}}>
                    <View style={{backgroundColor: '#b28bec10', height: this.props.rowsHeight - 2*this.props.activityMargin - 4, justifyContent: 'center', alignItems: 'center'}}>
                        <Text onLongPress={() => {this.props.online ? this.props.onActivitylongPress(item.key) : null}} style={{textAlign: 'center', color: this.props.theme ? 'black' : '#efefef'}}>{' ' + item.name + '\n' + item.detail}</Text>
                    </View>
                </View>)}
            </View>)
    }
}
