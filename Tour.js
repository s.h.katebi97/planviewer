import React, {Component} from 'react'
import {View, Platform, Text, TextInput, TouchableNativeFeedback, FlatList, Image, Alert, ScrollView, Dimensions} from 'react-native'
import { Button } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';

let screenHeight = Dimensions.get('window').height
let screenWidth = Dimensions.get('window').width

export class Tour extends Component{
    constructor(props){
        super(props)
        this.state={
            sideMenu: false,
            step: 1,
            name: "",
            timeout: 0,
            id: 0,
            show: true,
        }
    }

    render(){
        let styles = {
            overlay:{
                position: 'absolute',
                bottom: 10,
                left: 10,
                width: screenWidth - 20,
                borderRadius: 10, 
                backgroundColor: '#000000a0',//'#b28bec80',
            },
            container:{
                backgroundColor:  '#6200ee',// : '#b28bec30',
                flexDirection: 'row',
                width: screenWidth,
            },
            image:{
                margin: 40,
                backgroundColor: this.props.theme ? '#e5e5e5' : '#000000',
                flex: 1,
                width: screenWidth - 80,
                height: screenHeight - 133,
                resizeMode: 'contain',
            },
            step:{
                textAlign: 'center', 
                backgroundColor:'#55555500', 
                color:'#ffffff', 
                marginTop: 10,
                marginBottom: 2,
            },
            name:{
                textAlign: 'center', 
                backgroundColor:'#55555500', 
                color:'#ffffff',
                marginTop: 0,
                marginBottom: 10,
            },
            lastpageMainContainer:{
                backgroundColor: '#6200ee',// : '#b28bec30',
                width: screenWidth,
            },
            lastpageContainer:{
                marginTop: screenHeight/2 - 30,
                justifyContent: 'center', 
                alignItems: 'center', 
                flex: 1,
                width: screenWidth - 20,
                backgroundColor: '#00000000',//'#b28bec80',
            },
            guide:{
                fontSize: 30, 
                textAlign: 'center', 
                backgroundColor:'#55555500', 
                color: '#ffffff', 
                marginBottom: 30,
            },
            buttonContainer:{
                flex: 1,
            },
            button: {
                fontSize: 30, 
            },
        }
        return(
        <View style={styles.mainContainer}>
            <ScrollView pagingEnabled horizontal>
                <View style={styles.container}>
                    <LinearGradient colors={['#6200ee', '#ff9d00']} style={{flexDirection: 'row', width: screenWidth}}>
                        <Image style={styles.image} source={require('./Guide/Sources/Edited/1-Login.png')} />
                        <View style={styles.overlay}>
                            <Text style={styles.step}>First Stage</Text>
                            <Text style={styles.name}>Login Page</Text>
                        </View>
                    </LinearGradient>
                </View>
                <View style={styles.container}>
                    <LinearGradient colors={['#6200ee', '#ff9d00']} style={{flexDirection: 'row', width: screenWidth}}>
                        <Image style={styles.image} source={require('./Guide/Sources/Edited/2-Register.png')} />
                        <View style={styles.overlay}>
                            <Text style={styles.step}>Second Stage</Text>
                            <Text style={styles.name}>Register Page</Text>
                        </View>
                    </LinearGradient>
                </View>
                <View style={styles.container}>
                    <LinearGradient colors={['#6200ee', '#ff9d00']} style={{flexDirection: 'row', width: screenWidth}}>
                        <Image style={styles.image} source={require('./Guide/Sources/Edited/3-Plans.png')} />
                        <View style={styles.overlay}>
                            <Text style={styles.step}>Third Stage</Text>
                            <Text style={styles.name}>Plans Page</Text>
                        </View>
                    </LinearGradient>
                </View>
                <View style={styles.container}>
                    <LinearGradient colors={['#6200ee', '#ff9d00']} style={{flexDirection: 'row', width: screenWidth}}>
                        <Image style={styles.image} source={require('./Guide/Sources/Edited/4-SeeSharedPlan.png')} />
                        <View style={styles.overlay}>
                            <Text style={styles.step}>Fourth Stage</Text>
                            <Text style={styles.name}>Shared Plan Page</Text>
                        </View>
                    </LinearGradient>
                </View>
                <View style={styles.container}>
                    <LinearGradient colors={['#6200ee', '#ff9d00']} style={{flexDirection: 'row', width: screenWidth}}>
                        <Image style={styles.image} source={require('./Guide/Sources/Edited/5-SideMenu.png')} />
                        <View style={styles.overlay}>
                            <Text style={styles.step}>Fifth Stage</Text>
                            <Text style={styles.name}>SideMenu Page</Text>
                        </View>
                    </LinearGradient>
                </View>
                <View style={styles.container}>
                    <LinearGradient colors={['#6200ee', '#ff9d00']} style={{flexDirection: 'row', width: screenWidth}}>
                        <Image style={styles.image} source={require('./Guide/Sources/Edited/6-Notifications.png')} />
                        <View style={styles.overlay}>
                            <Text style={styles.step}>Sixth Stage</Text>
                            <Text style={styles.name}>Notifications Page</Text>
                        </View>
                    </LinearGradient>
                </View>
                <View style={styles.container}>
                    <LinearGradient colors={['#6200ee', '#ff9d00']} style={{flexDirection: 'row', width: screenWidth}}>
                        <Image style={styles.image} source={require('./Guide/Sources/Edited/7-News.png')} />
                        <View style={styles.overlay}>
                            <Text style={styles.step}>Seventh Stage</Text>
                            <Text style={styles.name}>News Page</Text>
                        </View>
                    </LinearGradient>
                </View>
                <View style={styles.container}>
                    <LinearGradient colors={['#6200ee', '#ff9d00']} style={{flexDirection: 'row', width: screenWidth}}>
                        <Image style={styles.image} source={require('./Guide/Sources/Edited/8-Activities.png')} />
                        <View style={styles.overlay}>
                            <Text style={styles.step}>Eighth Stage</Text>
                            <Text style={styles.name}>Activities Page</Text>
                        </View>
                    </LinearGradient>
                </View>
                <View style={styles.container}>
                    <LinearGradient colors={['#6200ee', '#ff9d00']} style={{flexDirection: 'row', width: screenWidth}}>
                        <Image style={styles.image} source={require('./Guide/Sources/Edited/9-AddAccess.png')} />
                        <View style={styles.overlay}>
                            <Text style={styles.step}>Ninth Stage</Text>
                            <Text style={styles.name}>Add Access Page</Text>
                        </View>
                    </LinearGradient>
                </View>
                <View style={styles.container}>
                    <LinearGradient colors={['#6200ee', '#ff9d00']} style={{flexDirection: 'row', width: screenWidth}}>
                        <Image style={styles.image} source={require('./Guide/Sources/Edited/10-AddActivity.png')} />
                        <View style={styles.overlay}>
                            <Text style={styles.step}>Tenth Stage</Text>
                            <Text style={styles.name}>Add Activity Page</Text>
                        </View>
                    </LinearGradient>
                </View>
                <View style={styles.lastpageMainContainer}>
                    <LinearGradient colors={['#6200ee', '#ff9d00']} style={{ flex: 1, flexDirection: 'row', width: screenWidth}}>
                        <View style={styles.lastpageContainer}>
                            <Text style={styles.guide}>Ready To Go?</Text>
                            <View style={styles.buttonContainer}>
                                <Button onPress={this.props.onTourCompelete} style={styles.button} title={"Yes!"}/>
                            </View>
                        </View>
                    </LinearGradient>
                </View>
            </ScrollView>
        </View>
        )
    }
}

