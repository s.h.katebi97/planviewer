import React, {Component} from 'react'
import {View, Platform, Text, TouchableNativeFeedback, FlatList} from 'react-native'

export class SideMenu extends Component{
    constructor(props){
        super(props)
    }

    render(){
      let styles = {
        container: {
          position: "absolute", 
          top: 65, 
          left: 0, 
          backgroundColor: "#ababab80", 
          width: this.props.screenWidth, 
          height: this.props.screenHeight
        },
        sideMenu:{
          container:{
            width: this.props.screenWidth-100,
            backgroundColor: this.props.theme ? '#12121200' : "#ffffff00",
          },
          common:{
            width: this.props.screenWidth - 100,
            flex: 1,
            backgroundColor: this.props.theme ? '#e5e5e5' : '#000000',
          },
          elements:{
            container:{
              flex: 1,
              width: this.props.screenWidth - 100,
              marginVertical: 1,
              backgroundColor: this.props.theme ? '#ffffff' : '#232323'
            },
            touchableArea:{
              padding: 10,
              backgroundColor: this.props.theme ? '#3700b3f0' : '#b28bec19', /* #2196F3 */
            },
            context:{
              padding: 10,
              color: this.props.theme ? 'black' : 'white',
            },
          },
        },
      }
      
      return(
        <View style={styles.container}>
          <FlatList horizontal={this.props.orientation} style={styles.sideMenu.common} data={this.props.sideMenuContent} extraData={this.props.online} contentContainerStyle={styles.sideMenu.container} keyExtractor={(item) => item.name} renderItem={(item) => {
            return (
              <View style={styles.sideMenu.elements.container}>
                <TouchableNativeFeedback
                    onPress={() => !item.item.connection || this.props.online ? item.item.onClick() : null}
                    background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
                  <View style={{...styles.sideMenu.elements.touchableArea, backgroundColor: !item.item.connection || this.props.online ? this.props.theme ? '#3700b308' : '#b28bec19' : this.props.theme ? 'darkgrey' : '#12121219'}}>
                    <Text style={{...styles.sideMenu.elements.context, color: !item.item.connection || this.props.online ? this.props.theme ? 'black' : 'white' : 'grey'}}>{item.item.name}</Text>
                  </View>
                </TouchableNativeFeedback>
              </View>
            )
          }}/>
        </View>
      )
    }
}