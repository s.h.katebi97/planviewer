import React, {Component} from 'react'
import {View, Platform, TextInput, Text, ScrollView, Dimensions, TouchableNativeFeedback, ToastAndroid, Alert} from 'react-native'
import { SideMenu } from './SideMenu'

let screenHeight = Dimensions.get('window').height
let screenWidth = Dimensions.get('window').width

export class NotificationManager extends Component{
    constructor(props){
        super(props)
        this.state={
            context: "",
            timeout: 0,
            sideMenu: false,
          }
      }
  
    sideMenu = () => this.setState({sideMenu: !this.state.sideMenu})

    render(){
        let styles = {
            container: {
                width: screenWidth, 
                flex: 1,
                backgroundColor: this.props.theme ? '#e5e5e5' : '#000000',
            },
            header:{
                overlay:{
                    backgroundColor: this.props.theme ? '#6200eeff' : '#b28bec30',
                    flexDirection: 'row',
                    width: screenWidth,
                },
                container:{
                    backgroundColor: this.props.theme ? '#ededed' : '#121212', //'#b28bec',
                    elevation: 5,
                },
                title:{
                    flex: 10,
                    color: 'white',
                    fontSize: 30,
                    marginLeft: 10,
                    marginBottom: 5,
                    paddingVertical: 10,
                },
                connection:{
                    flex: 10,
                    color: 'white',
                    fontSize: 30,
                    marginLeft: 10,
                    marginBottom: 5,
                    paddingVertical: 10,
                },
                button:{
                    container:{
                        marginLeft: 10,
                        flex: 1, 
                        justifyContent: 'center', 
                        alignItems: 'center', 
                    },
                    context:{
                        fontSize: 35,
                        fontFamily: "Material-Design-Iconic-Font",
                        color: 'white',
                    }
                }
            },
            add:{
                container:{
                    flexDirection: 'row',
                    marginHorizental: 10,
                },
                context:{
                    flex: 4, 
                    backgroundColor:'#55555500', 
                    color:this.props.theme ?  '#121212' : '#aaaaaa', 
                    marginLeft: 10, 
                    marginRight: 5, 
                    marginVertical: 10, 
                    borderColor: this.props.theme ? '#bb86fc' : '#00dbc5', 
                    borderWidth: 0, 
                    borderBottomWidth:2,
                },
                timeout:{
                    flex: 1, 
                    backgroundColor:'#55555500', 
                    color:this.props.theme ?  '#121212' : '#aaaaaa', 
                    marginLeft: 5, 
                    marginRight: 5, 
                    marginVertical: 10, 
                    borderColor: this.props.theme ? '#bb86fc' : '#00dbc5', 
                    borderWidth: 0, 
                    borderBottomWidth:2, 
                    textAlign: 'center',
                },
                button:{
                    container:{
                        flex: 1, 
                        justifyContent: 'center', 
                        alignItems: 'center', 
                        backgroundColor: '#00000000', 
                        marginLeft: 5, 
                        marginRight: 10, 
                        marginVertical: 10, 
                    },
                    touchableArea:{
                        marginHorizental: 2,  
                        backgroundColor:  this.props.theme ? '#bb86fc' : '#00dbc5', /* #2196F3 */ 
                        borderRadius: 10,
                    },
                    context:{
                        fontSize: 30, 
                        fontFamily: "Material-Design-Iconic-Font", 
                        padding: 10, 
                        color: 'black',
                    },
                },
            },
            notifications:{
                container:{
                    width: screenWidth - 20,
                    margin: 10,
                },
                elements:{
                    container:{
                        width: screenWidth - 20, 
                        marginVertical: 5, 
                        marginHorizental: 10, 
                        borderRadius: 10, 
                        backgroundColor: this.props.theme ? '#ffffff' : '#121212',
                    },
                    overlay:{
                        width: screenWidth - 20,
                        borderRadius: 10, 
                        backgroundColor: this.props.theme ? '#b28bec00' : '#b28bec20',
                    },
                    context:{
                        textAlign: 'center', 
                        backgroundColor:'#55555500', 
                        color:this.props.theme ?  '#121212' : '#aaaaaa', 
                        marginTop: 10,
                    },
                    timeoutAndDelete:{
                        container:{
                            height: 70,
                        },
                        delete:{
                            container:{
                                position: 'absolute', 
                                top: 0, 
                                right: 0,
                            },
                            button:{
                                container:{
                                    flex: 1, 
                                    justifyContent: 'center', 
                                    alignItems: 'center', 
                                    backgroundColor: '#00000000', 
                                    margin: 10,
                                },
                                touchableArea:{
                                    marginVertical: 2, 
                                    backgroundColor:  this.props.theme ? '#bb86fc' : '#00dbc5', /* #2196F3 */ 
                                    borderRadius: 10,
                                },
                                context:{
                                    fontSize: 30, 
                                    fontFamily: "Material-Design-Iconic-Font", 
                                    padding: 10, 
                                    color: 'black',
                                },
                            },
                        },
                        timeout:{
                            duration:{
                                container:{
                                    position: 'absolute', 
                                    top: 0, 
                                    left: 5,
                                },
                                textArea:{
                                    flex: 1, 
                                    justifyContent: 'center', 
                                    alignItems: 'center', 
                                    backgroundColor: '#00000000', 
                                    margin: 10,
                                },
                                context:{
                                    fontSize: 30, 
                                    fontFamily: "Material-Design-Iconic-Font", 
                                    padding: 10, 
                                    color: this.props.theme ? 'black' : 'white',
                                },
                            },
                            sign:{
                                container:{
                                    position: 'absolute', 
                                    top: 0, 
                                    left: 35,
                                },
                                textArea:{
                                    flex: 1, 
                                    justifyContent: 'center', 
                                    alignItems: 'center', 
                                    backgroundColor: '#00000000', 
                                    margin: 10,
                                },
                                context:{
                                    color: this.props.theme ? 'black' : 'white',
                                },
                            },
                        },
                    },
                },
            },
        }
        return(
            <View style={styles.container}>
                <View style={styles.header.container}>
                    <View style={styles.header.overlay}>
                        <View style={styles.header.button.container}>
                        <TouchableNativeFeedback
                            onPress={this.sideMenu}
                            background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
                            <View>
                                <Text style={styles.header.button.context}>{"\uf197"}</Text>
                            </View>
                        </TouchableNativeFeedback>
                        </View>
                        <Text style={styles.header.title}>Notfications</Text>
                    </View>
                </View>
                {this.props.online ? <View style={styles.add.container}>
                    <TextInput style={styles.add.context} placeholder={"Context"} placeholderTextColor={styles.add.context.color} onChangeText={(newContext) => this.setState({context: newContext})}/>
                    <TextInput style={styles.add.timeout} placeholder={"Timeout"} keyboardType={"numeric"} placeholderTextColor={styles.add.timeout.color} onChangeText={(newTimeout) => this.setState({timeout: Number(newTimeout)})}/>
                    <View style={styles.add.button.container}>
                        <TouchableNativeFeedback
                            onPress={() => (this.state.context==='' || this.state.timeout===0) ? ToastAndroid.showWithGravityAndOffset('Please Fill The Requested Field', ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50) : this.props.onAdd(this.state.context, this.state.timeout)}
                            background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
                            <View style={styles.add.button.touchableArea}>
                                <Text style={styles.add.button.context}>{"\uf1fa"}</Text>
                            </View>
                        </TouchableNativeFeedback>
                    </View>
                </View> : null}
                <ScrollView style={styles.notifications.container}>
                    {this.props.notifications.map((element) => 
                    <View style={styles.notifications.elements.container}>
                        <View style={styles.notifications.elements.overlay}>
                            <Text style={styles.notifications.elements.context}>{element.context}</Text>
                            <View style={styles.notifications.elements.timeoutAndDelete.container}>
                                {this.props.online ? <View style={styles.notifications.elements.timeoutAndDelete.delete.container}>
                                    <View style={styles.notifications.elements.timeoutAndDelete.delete.button.container}>
                                        <TouchableNativeFeedback
                                            onPress={() => {
                                                Alert.alert(`Delete Activity`,
                                                    `Do You Realy Want To Delete Notfication ${element.context}`,[
                                                        {text: "No", style:"cancel"},
                                                        {text: "Yes", onPress: () => this.props.onDeleteNotification(element.id)}
                                                    ])
                                                }
                                            }
                                            style={{marginRight: 10}}
                                            background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
                                            <View style={styles.notifications.elements.timeoutAndDelete.delete.button.touchableArea}>
                                                <Text style={styles.notifications.elements.timeoutAndDelete.delete.button.context}>{"\uf154"}</Text>
                                            </View>
                                        </TouchableNativeFeedback>
                                    </View>
                                </View> : null}
                                <View style={styles.notifications.elements.timeoutAndDelete.timeout.duration.container}>
                                    <View style={styles.notifications.elements.timeoutAndDelete.timeout.duration.textArea}>
                                        <Text style={styles.notifications.elements.timeoutAndDelete.timeout.duration.context}>{"\uf334"}</Text>
                                    </View>
                                </View>
                                <View style={styles.notifications.elements.timeoutAndDelete.timeout.sign.container}>
                                    <View style={styles.notifications.elements.timeoutAndDelete.timeout.sign.textArea}>
                                        <Text style={styles.notifications.elements.timeoutAndDelete.timeout.sign.context}>{element.timeout}</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>)}
                </ScrollView>
                {this.state.sideMenu === true ?(<SideMenu online={this.props.online} orientation={this.props.orientation} screenWidth={screenWidth} screenHeight={screenHeight} sideMenuContent={this.props.sideMenuContent} theme={this.props.theme} />):null}
            </View>
        )
    }
}

