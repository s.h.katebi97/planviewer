import React, { Component } from 'react'
import { View, TextInput, Dimensions } from 'react-native'

let screenHeight = Dimensions.get('window').height
let screenWidth = Dimensions.get('window').width

export class Login
 extends Component{
    constructor(props){
        super(props)
        this.state={

        }
    }

    render(){
        let styles = {
            container:{
                paddingHorizontal: 10,
                justifyContent: 'space-around',
                flex: 1,
                backgroundColor: this.props.theme ? '#ffffff' : '#121212',
                width: screenWidth - 20,
                height: screenHeight - 500,
                borderRadius: 10,
            },
            username:{
                borderColor:this.props.theme ? '#bb86fc' : '#00dbc5', 
                borderWidth: 0, 
                borderBottomWidth:2, 
                color:this.props.theme ? '#121212' : '#aaaaaa', 
                textAlign: 'center',
                margin: 10, 
            },
            password:{
                borderColor:this.props.theme ? '#bb86fc' : '#00dbc5', 
                borderWidth: 0, 
                borderBottomWidth:2, 
                color:this.props.theme ? '#121212' : '#aaaaaa', 
                textAlign: 'center',
                margin: 10, 
                marginBottom: 40,
            }
        }
        return(
            <View style={styles.container}>
                <TextInput style={styles.username} placeholderTextColor={styles.username.color} placeholder="Username" onChangeText={this.props.updateUsername} value={this.props.username} blurOnSubmit/>
                <TextInput style={styles.password} placeholderTextColor={styles.password.color} placeholder="Password" onChangeText={this.props.updatePassword} value={this.props.password} blurOnSubmit/>
          </View>
        )
    }
}
