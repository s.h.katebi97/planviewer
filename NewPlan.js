import React, {Component} from 'react'
import {View, TextInput, Dimensions, TouchableNativeFeedback, Platform, Text} from 'react-native'

let screenHeight = Dimensions.get('window').height
let screenWidth = Dimensions.get('window').width

export class NewPlan extends Component{
    constructor(props){
        super(props)
        this.state={
            name: "",
            disableButton: false,
        }
    }
    
    render(){
        let styles = {
            container:{
                flex: 1,
                backgroundColor: this.props.theme ? '#e5e5e5' : '#000000',
                width: screenWidth,
            },
            scrollAreaStyle:{
                flex: 1,
                backgroundColor: this.props.theme ? '#e5e5e5' : '#000000',
                width: screenWidth,
            },
            inputStyle:{
                borderColor:this.props.theme ? '#bb86fc' : '#00dbc5', 
                borderWidth: 0, 
                borderBottomWidth:2, 
                color:'#aaaaaa', 
                width: screenWidth - 20,
                textAlign: 'center',
                marginLeft: 10, 
                marginRight: 10, 
                marginBottom: 10, 
                marginTop: 10,
            },
            buttonStyle:{
                height: 50,
                width: 50,
                backgroundColor: this.props.theme ? '#bb86fc' : '#00dbc5', /* #2196F3 */ 
                borderRadius: 25,
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
            },
            buttonContainerStyle: {
                position: 'absolute',
                right: 20,
                bottom: 20,
                justifyContent: 'center',
            },
            backButtonContainerStyle: {
                position: 'absolute',
                left: 20,
                bottom: 20,
                justifyContent: 'center',
            },
            context:{
                fontSize: 20, 
                fontFamily: "Material-Design-Iconic-Font", 
                color: 'black',
            },
        }
        return(
            <View style={styles.container}>
                <View style={styles.scrollAreaStyle}>
                    <TextInput onChangeText={(newName)=>this.setState({name: newName})} placeholderTextColor={styles.inputStyle.color} style={styles.inputStyle} placeholder={"Plan Name"}/>
                </View>
                <View style={styles.buttonContainerStyle}>
                    <TouchableNativeFeedback
                        disabled={this.state.disableButton}
                        onPress={() => this.setState({disableButton: true}, this.props.onFinish(this.state.name))}
                        background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
                        <View style={styles.buttonStyle}>
                            <Text style={styles.context}>{"\uf2ee"}</Text>
                        </View>
                    </TouchableNativeFeedback>
                </View>
                <View style={styles.backButtonContainerStyle}>
                    <TouchableNativeFeedback
                        disabled={this.state.disableButton}
                        onPress={() => this.setState({disableButton: true}, this.props.onBack())}
                        background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
                        <View style={styles.buttonStyle}>
                            <Text style={styles.context}>{"\uf118"}</Text>
                        </View>
                    </TouchableNativeFeedback>
                </View>
            </View>
        )
    }
}
