/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage'
import Modal from "react-native-modal"
import { CheckBox, ButtonGroup } from 'react-native-elements'
import { Alert, Platform, StyleSheet, Text, View, ScrollView, TextInput, FlatList, TouchableNativeFeedback, /* AsyncStorage, */ Dimensions, Image } from 'react-native';
import { Bar, Circle, CircleSnail, Pie } from 'react-native-progress'
import axios from 'axios'
import  PushNotification from 'react-native-push-notification'
import { ActivityRow } from './ActivityRow.js';
import { NewActivity } from './NewActivity.js';
import { NewPlan } from './NewPlan.js'
import { NewAccess } from './NewAccess.js'
import { NotificationManager } from './ManageNotifications.js'
import { PlanManager } from './ManagePlan.js'
import { Register } from './Register.js'
import { Login } from './Login.js'
import { NewsManager } from './ManageNews.js'
import { Tour } from './Tour.js'
global.Buffer = require('buffer').Buffer;

let stages = {
  Tour: 0,
  Auth: 1,
  Loading: 2,
  Plans: 3,
  Activities: 4,
  AddMenu: 5,
  AddPlan:6,
  addActivity:7,
  AddAccess: 8,
  ManageNotifications: 9,
  ManageNews: 10,
  Uploading: 11,
}
let global_server_path= /*"http://10.0.3.2:8000",*/"https://universityproject98.herokuapp.com"
let templateText = `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`
let DimensionsError = 23.999982561383944812405
let screenHeight = Dimensions.get('window').height
let screenWidth = Dimensions.get('window').width
let rows = {
  width: 50,
  height: screenHeight/8,
  horizentalmargin: 0,
}
let initialStep = stages.Auth
let i = 0
let activityMargin = 2
let daysOfWeekIndicators= {
  width: 80,
  verticalmargin: 0,
  diffrenceBetweenButtonAndText: 10,
}

export default class App extends Component {
  constructor(props){
    super(props)
    this.state={
      sideMenuContent:[
        {name: "Plans", connection: false, onClick: () => this.getPlans()},
        {name: "Notifications", connection: false, onClick: () => this.getServerNotification(9)},
        {name: "News", connection: false, onClick : () => this.getNews()},
        {name: "Toggle Theme", connection: false, onClick : () => this.setState({theme: !this.state.theme}, () => {AsyncStorage.setItem('theme', this.state.theme.toString())})},
        {name: "Settings", connection: false, onClick: () => this.setState({settings: true})},
        {name: "Logout", connection: true, onClick: () => this.logout()},
      ],
      news:[
        {id: 2, caption: templateText, picture:"https://universityproject98.herokuapp.com/news/picture?token=d1e9a4b80049be334fc15e7db9a27478c0b1fcb80b524e4d5ab0710a8c49fc9c&id=2&picture=abb0c37110507bd1d3f5ec3a0641c6f7", timeout: 10},
        {id: 3, caption: templateText, picture:"https://universityproject98.herokuapp.com/news/picture?token=d1e9a4b80049be334fc15e7db9a27478c0b1fcb80b524e4d5ab0710a8c49fc9c&id=2&picture=2b06c387f9d49d2920c5998ecf88d52b", timeout: 20},
      ],
      uploadProgress: 0,
      hideOrNot: false,
      token: "",
      prefix: "",
      first: "",
      last: "",
      middle: "",
      email: "",
      username: "",
      password: "",
      register: false,
      step: initialStep, /* 1: Auth, 2: Loading, 3: Plans, 4: Activities, 5: Add Menu, 6: Add Plan, 7: Add Activity, 8: Add Access, 9: Manage Notifications, 10: Manage News */
      plans: [{id: 0, name: "Template"}],
      timesOfDays:[],
      weeks: [],
      activities: [],
      serverNotification: [],
      orientation: false,
      addPlanPreiviousStep: 4,
      requested_plan_id: 0,
      server_path: global_server_path,
      activityTypes: [{id: 1, name: "Normal", supporter: "Main Type"}],
      activityPriorities: [{id: 1, name: "Normal", level: 5 }],
      online: true,
      theme: true,
      settings: false,
      selectedServerType: 0,
      position: 0
    }
    this.scrolls = {}
    var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
    var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
    Date.prototype.getMonthName = function() {
        return months[ this.getMonth() ];
    };
    Date.prototype.getDayName = function() {
        return days[ this.getDay() ];
    };
    Date.prototype.isLeapYear = function() {
      var year = this.getFullYear();
      if((year & 3) != 0) return false;
      return ((year % 100) != 0 || (year % 400) == 0);
    };
    Date.prototype.getDOY = function() {
      var dayCount = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334];
      var mn = this.getMonth();
      var dn = this.getDate();
      var dayOfYear = dayCount[mn] + dn;
      if(mn > 1 && this.isLeapYear()) dayOfYear++;
      return dayOfYear;
    };
  }

  onTourCompelete = () => {
    AsyncStorage.setItem('first', 'true').then(() => {this.setState({step: initialStep})})
  }

  setAllScrolls = (event) => {
    let x
    x = event !== undefined ? event.nativeEvent.contentOffset.x : this.state.x
    for (let scroll in this.scrolls)
      this.scrolls[scroll].scrollTo({x: x, y: 0, animated: true})
    this.setState({x: x})
  }

  checkConnectivity = () => {
    fetch(this.state.server_path + '/now')
      .then((response) => {
        try{
          return response.json()
        }
        catch(err){
          console.error(err)
          return undefined
        }
      })
      .then((responseJson) => {
        this.setState({online: responseJson.message === 'Successful',})
      })
      .catch((error) => {
        this.setState({online: false,})
      })
  }

  deleteNews = (newsID) => {
    return this.setState({step: 2}, () => fetch(this.state.server_path + '/news/delete', {
      method: "DELETE",
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        token: this.state.token,
        id: newsID
      })
    })
    .then((response) => {
      try{
        return response.json()
      }
      catch(err){
        return undefined
      }
    })
    .then((responseJson) => {
      this.getNews()
    })
    .catch((err) => {
      console.error(err)
    }))
  }

  addPicture = (id, picture, callback) => { 
    this.setState({step: stages.Uploading},() => {
      const data = new FormData()
      data.append('token', this.state.token)
      data.append('id', id)
      data.append('picture', picture)
      axios.post(this.state.server_path+'/news/picture', data,{
        onUploadProgress: (progressEvent) => {
          const totalLength = progressEvent.lengthComputable ? progressEvent.total : progressEvent.target.getResponseHeader('content-length') || progressEvent.target.getResponseHeader('x-decompressed-content-length')
          this.setState({uploadProgress: progressEvent.loaded/totalLength})
        }})
        .then((res) => {
          this.setState({uploadProgress: 1})
          return Promise.all([res.status, res.data])
        })
        .then(([status, res]) => {
          if(status)
            res.then((data) => callback(undefined, data, () => this.getNews()))
          else
            res.then((data) => callback(data, undefined, () => this.getNews()))
        })
        .catch(error => {
          return callback(error, undefined, () => this.getNews())
        });
    })
  }

  addNews = (name, callback) => {
    fetch(this.state.server_path + '/news/create', {
      method: "POST",
      headers: {
        Accept: 'application/json; charset=utf-8',
        'Content-Type': 'application/json; charset=utf-8',
      },
      body: JSON.stringify({
        token: this.state.token,
        text: name,
      })
    })
    .then((response) => {
      try{
        return [true, response.json()]
      }
      catch(err){
        return [false, err]
      }
    })
    .then((responseJson) => {
      if(responseJson[0])
        return responseJson[1].then((data) => callback(undefined, data))
      else
        return callback(responseJson[1], undefined)
    })
    .catch((error) => {
      callback(error, undefined)
    })
  }

  setNewsTimeout = (id, timeout, callback) => {
    fetch(this.state.server_path + '/news/timeout', {
      method: "POST",
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        token: this.state.token,
        id: id,
        timeout: timeout,
      })
    })
    .then((response) => {
      try{
        return [true, response.json()]
      }
      catch(err){
        return [false, err]
      }
    })
    .then((responseJson) => {
      if(responseJson[0])
        return callback(undefined, responseJson[1])
      else
        return callback(responseJson[1], undefined)
    })
    .catch((error) => {
      callback(error, undefined)
    })
  }

  getNews = () => {
    if(this.state.online)
      this.setState({step: 2}, () => fetch(this.state.server_path + '/news/all?token=' + this.state.token)
        .then((response) => {
          try{
            return Promise.all([response.status, response.json()])
          }
          catch(err){
            console.error(err)
            return undefined
          }
        })
        .then(([status, responseJson]) => {
          if(status === 200){
            AsyncStorage.setItem('news', JSON.stringify(responseJson.news))
            this.setState({news: responseJson.news.map(element => {
              return  {
                id: element.id, 
                caption: element.caption, 
                timeout: element.timeout, 
                picture: this.state.server_path+'/news/picture?token='+this.state.token+'&id='+element.id+'&picture='+element.picture
              }
            })}, () => this.setState({step: 10}))
          }
          else if(status === 404){
            Alert.alert("Get News Failed", "You Don't Have Any News")
            AsyncStorage.setItem('news', JSON.stringify([]))
            this.setState({news: []}, () => this.setState({step: 10}))
          }
          else if(status === 500){
            Alert.alert("Internal Server Error", "Please Retry In A Minute.")
            AsyncStorage.getItem('news').then((data) => this.setState({news: JSON.parse(data).map(element => {
              return  {
                id: element.id, 
                caption: element.caption, 
                timeout: element.timeout, 
                picture: this.state.server_path+'/news/picture?token='+this.state.token+'&id='+element.id+'&picture='+element.picture
              }
            })}, () => this.setState({step: 10}))).catch((err) => Alert.alert(err))
          }
          else if (status === 400){
            Alert.alert("App Internal Error", "Please Test Below Option:\n1) Close The App And Reopen it.(Make Sure That The Closed)\n2)If This Doesn't Work, Clear Cache And If That Doesn't Work Too, Clear Data The App.\n3)If None of Above Workd, Contact Support.")
            AsyncStorage.getItem('news').then((data) => this.setState({news: JSON.parse(data).map(element => {
              return  {
                id: element.id, 
                caption: element.caption, 
                timeout: element.timeout, 
                picture: this.state.server_path+'/news/picture?token='+this.state.token+'&id='+element.id+'&picture='+element.picture
              }
            })}, () => this.setState({step: 10}))).catch((err) => Alert.alert(err))
          }
        })
        .catch((error) => {
          this.setState({step: 2, loadingMessage: "There Is A Network Error", online: false,}, this.getNews)
        })
      )
    else
      AsyncStorage.getItem('news').then((data) => this.setState({news: JSON.parse(data).map(element => {
        return  {
          id: element.id, 
          caption: element.caption, 
          timeout: element.timeout, 
          picture: this.state.server_path+'/news/picture?token='+this.state.token+'&id='+element.id+'&picture='+element.picture
        }
      })}, () => this.setState({step: 10}))).catch((err) => Alert.alert(err))
  }

  addServerNotification = (context, timeout) => {
    this.setState({step: 2}, () => fetch(this.state.server_path + '/notification/create', {
      method: "POST",
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        token: this.state.token,
        context: context,
        timeout: timeout
      })
    })
    .then((response) => {
      try{
        return response.json()
      }
      catch(err){
        console.error(err)
        return undefined
      }
    })
    .then((responseJson) => {
      return this.getServerNotification(9)
    })
    .catch((error) => {
      console.error(error)
    }))
  }

  getServerNotification = (nextStep) => {
    if(this.state.online)
      this.setState({step: 2}, () => fetch(this.state.server_path + '/notification/all?token=' + this.state.token)
        .then((response) => {
          try{
            return Promise.all([response.status, response.json()])
          }
          catch(err){
            console.error(err)
            return undefined
          }
        })
        .then(([status, responseJson]) => {
          if(status === 200){
            AsyncStorage.setItem('notification', JSON.stringify(responseJson.notification))
            this.setState({serverNotification: responseJson.notification}, () => this.setState({step: nextStep}))
          }
          else if(status === 404){
            Alert.alert("Get Notifications Failed", "You Don't Have Any Notifications")
            AsyncStorage.setItem('notification', JSON.stringify([]))
            this.setState({serverNotification: []}, () => this.setState({step: nextStep}))
          }
          else if(status === 500){
            Alert.alert("Internal Server Error", "Please Retry In A Minute.")
            AsyncStorage.getItem('notification').then((data) => this.setState({serverNotification: JSON.parse(data)}, () => this.setState({step: nextStep}))).catch((err) => Alert.alert(err))
          }
          else if (status === 400){
            Alert.alert("App Internal Error", "Please Test Below Option:\n1) Close The App And Reopen it.(Make Sure That The Closed)\n2)If This Doesn't Work, Clear Cache And If That Doesn't Work Too, Clear Data The App.\n3)If None of Above Workd, Contact Support.")
            AsyncStorage.getItem('notification').then((data) => this.setState({serverNotification: JSON.parse(data)}, () => this.setState({step: nextStep}))).catch((err) => Alert.alert(err))
          }
        })
        .catch((error) => {
          this.setState({step: 2, loadingMessage: "There Is A Network Error", online: false,}, () => this.getServerNotification(nextStep))
        })
      )
    else
      AsyncStorage.getItem('notification').then((data) => this.setState({serverNotification: JSON.parse(data)}, () => this.setState({step: nextStep}))).catch((err) => Alert.alert(err))
  }

  deleteServerNotification = (id) => {
    this.setState({step: 2}, () => fetch(this.state.server_path + '/notification/delete', {
      method: "DELETE",
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        token: this.state.token,
        id: id
      })
    })
    .then((response) => {
      try{
        return response.json()
      }
      catch(err){
        console.error(response)
        return undefined
      }
    })
    .then((responseJson) => {
      return this.getServerNotification(9)
    })
    .catch((err) => {
      console.error(err)
    }))
  }

  deletePlan = (planShareLink) => {
    return fetch(this.state.server_path + '/plan/delete', {
      method: "DELETE",
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        token: this.state.token,
        share_link: planShareLink
      })
    })
    .then((response) => {
      try{
        return response.json()
      }
      catch(err){
        console.error(response)
        return undefined
      }
    })
    .then((responseJson) => {
      this.getPlans()
    })
    .catch((err) => {
      console.error(err)
    })
  }

  shareForAll = (planShareLink) => {
    return fetch(this.state.server_path + '/plan/share_to_all', {
      method: "POST",
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        token: this.state.token,
        share_link: planShareLink
      })
    })
    .then((response) => {
      try{
        return response.json()
      }
      catch(err){
        console.error(response)
        return undefined
      }
    })
    .then((responseJson) => {
      Alert.alert("Share Succeed", `${responseJson.message}`)
    })
    .catch((err) => {
      console.error(err)
    })
  }

  deleteActivity = (activityID) => {
    return fetch(this.state.server_path + '/activity/delete', {
      method: "DELETE",
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        token: this.state.token,
        plan: this.state.requested_plan_id,
        activity: activityID
      })
    })
      .then((response) => {
        try{
          return response.json()
        }
        catch(err){
          console.error(response)
          return undefined
        }
      })
      .then((responseJson) => {
        this.setState({step: 4}, this.getPlanActivities)
      })
      .catch((err) => {
        console.error(err)
      })
  }

  editActivity = (activityID) => {
    let activityName = this.state.activities.filter((elem) => {elem.id = activityID})
    Alert.alert("Delete Activity", `Do You Want to Delete Activity ${activityName} With ID ${activityID}`,
      [
        {text: "No" , style: 'cancel'},
        {text: "Yes", onPress: () => {this.deleteActivity(activityID)}},
      ]
    )
  }

  addAccess = (selectedPlan, selectedActivity, showDetail, showBrief) => {
    return fetch(this.state.server_path + '/activity/set_permission', {
      method: "POST",
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        token: this.state.token,
        plan: selectedPlan,
        activity: selectedActivity,
        brief_permission: showBrief,
        detail_permission: showDetail
      })
    })
      .then((response) => {
        try{
          return response.json()
        }
        catch(err){
          console.error(err)
          return undefined
        }
      })
      .then((responseJson) => {
        return true
      })
      .catch((error) => {
        console.error(error)
      })
  }

  addPlan = (newPlanName) => {
    fetch(this.state.server_path + '/plan/create', {
      method: "POST",
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        token: this.state.token,
        name: newPlanName,
      })
    })
      .then((response) => {
        try{
          return response.json()
        }
        catch(err){
          console.error(err)
          return undefined
        }
      })
      .then((responseJson) => {
        this.getPlans()
      })
      .catch((error) => {
        console.error(error)
      })
      
  }

  updatePlansAndActivityTypesAndPriorities = (nextStep) => {
    return this.setState({step: 2}, () => fetch(this.state.server_path + '/plan/all?token=' + this.state.token)
      .then((response) => {
        try{
          return response.json()
        }
        catch(err){
          console.error(err)
          return undefined
        }
      })
      .then((responseJson) => {
        let newArray = []
        for(i in responseJson.plans)
          newArray.push({
            id: responseJson.plans[i].id,
            name: responseJson.plans[i].name,
            link: responseJson.plans[i].share_link
          })
        this.setState({
          plans: newArray
        }, () => this.updateActivityTypesAndPriorities(nextStep))
      })
      .catch((error) => {
        console.error(error)
      })
    )
  }

  addActivity = (name, detail, selectedType, selectedPriority, selectedPeriod, startTime, endTime, startDate, endDate) => {
    console.warn("AddActivity Executed")
    fetch(this.state.server_path + '/activity/create', {
      method: "POST",
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        token: this.state.token,
        plan: this.state.requested_plan_id,
        activity:{
          name: name,
          detail: detail,
          type: selectedType,
          priority: selectedPriority,
          time:{
            start: startTime + ':00',
            end: endTime + ':00'
          },
          date:{
            start: startDate,
            end: endDate
          },
          period:{
            number: 1,
            repeat: selectedPeriod,
            position: 1
          }
        }
      })
    })
      .then((response) => {
        try{
          return Promise.all([response.status, response.json()])
        }
        catch(err){
          console.error(response)
          return undefined
        }
      })
      .then(([status, responseJson]) => {
        if(responseJson.message === "Activity Added Successfully."){
          console.warn("AddActivity Executed Successfully")
          return this.setState({step: stages.Activities}, this.getPlanActivities)
        }
        console.warn(`AddActivity Executed With Code ${status}`)
      })
      .catch((err) => {
        console.error(error)
      })
  }

  newAddActivity = (name, detail, selectedType, selectedPriority, selectedPeriod, startTime, endTime, startDate, endDate, selectedDays) => {
    let periods = []
    for (let day of selectedDays) periods.push({
      time:{
        start: startTime + ':00',
        end: endTime + ':00'
      },
      date:{
        start: startDate,
        end: endDate
      },
      numbers: 1,
      repeat: selectedPeriod,
      position: day,
    })
    fetch(this.state.server_path + '/activity/new_create', {
      method: "POST",
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        token: this.state.token,
        plan: this.state.requested_plan_id,
        activity:{
          name: name,
          detail: detail,
          type: selectedType,
          priority: selectedPriority,
          period: periods
        }
      })
    })
      .then((response) => {
        try{
          return Promise.all([response.status, response.json()])
        }
        catch(err){
          console.error(response)
          return undefined
        }
      })
      .then(([status, responseJson]) => {
        if(responseJson.message === "Activity Added Successfully."){
          console.warn("AddActivity Executed Successfully")
          return this.setState({step: stages.Activities}, this.getPlanActivities)
        }
        console.warn(`AddActivity Executed With Code ${status}`)
      })
      .catch((err) => {
        console.error(error)
      })
  }

  updateActivityTypes = (nextStep) => {
    return this.setState({step: 2}, () => fetch(this.state.server_path + '/activity/types?token=' + this.state.token + '&plan=' + this.state.requested_plan_id)
      .then((response) => {
        try{
          return response.json()
        }
        catch(err){
          console.error(err)
          return undefined
        }
      })
      .then((responseJson) => {
        this.setState({activityTypes: responseJson.types}, () => this.setState({step: nextStep}))
      })
      .catch((error) => {
        console.error(error)
      })
    )
  }

  updateActivityTypesAndPriorities = (nextStep) => {
    return this.setState({step: 2}, () => fetch(this.state.server_path + '/activity/priorities?token=' + this.state.token + '&plan=' + this.state.requested_plan_id)
      .then((response) => {
        try{
          return response.json()
        }
        catch(err){
          console.error(err)
          return undefined
        }
      })
      .then((responseJson) => {
        this.setState({activityPriorities: responseJson.priority}, () => this.updateActivityTypes(nextStep))
      })
      .catch((error) => {
        console.error(error)
      })
    )
  }

  updateWeekList = (event) => {
    if(Math.ceil(event.nativeEvent.contentOffset.y/(screenHeight - DimensionsError)) + 2 >= this.state.weeks.length){
      let previousWeekStartDate = new Date(this.state.weeks[this.state.weeks.length-1].startDate)
      let previousWeekEndDate = new Date(this.state.weeks[this.state.weeks.length-1].endDate)
      previousWeekStartDate.setDate(previousWeekStartDate.getDate() + 7)
      previousWeekEndDate.setDate(previousWeekEndDate.getDate() + 7)
      this.setState({
        weeks: [...this.state.weeks , {
          startDate: previousWeekStartDate,
          endDate: previousWeekEndDate,
          Saturday: [],
          Sunday: [],
          Monday: [],
          Tuesday: [],
          Wednesday: [],
          Thursday: [],
          Friday: [],
        }]
      }, this.updateGUI)
    }
  }

  getStartAndEndDateOfWeek = (dayOfWeek) => {
    let startOfWeek = new Date(dayOfWeek)
    startOfWeek.setDate((startOfWeek.getDay() !== 6) ?( startOfWeek.getDate() - startOfWeek.getDay() - 1 ): startOfWeek.getDate())
    let endOfWeek = new Date(startOfWeek)
    endOfWeek.setDate(endOfWeek.getDate() + 7)
    return {startOfWeek, endOfWeek}
  }

  checkDateExpiration = (startOfWeek, endOfWeek, startOfActivity, endOfActivity) => {
    let _startOfWeek = new Date(startOfWeek)
    let _endOfWeek = new Date(endOfWeek)
    let _startOfActivity = new Date(startOfActivity)
    let _endOfActivity = new Date(endOfActivity)
    return (_startOfWeek > _startOfActivity)?((_endOfActivity > _startOfWeek)?(true):(false)):((_endOfWeek > _startOfActivity)?(true):(false))
  }

  componentDidMount(){
    let tempArrays = []
    for(let i = 0; i < 25; i++)
      tempArrays.push({start:i,end:i+1})
    this.setState({timesOfDays:tempArrays})
    let {startOfWeek:s1, endOfWeek:e1} = this.getStartAndEndDateOfWeek(new Date())
    let {startOfWeek:s2, endOfWeek:e2} = this.getStartAndEndDateOfWeek(new Date())
    s2.setDate(s2.getDate() + 7)
    e2.setDate(e2.getDate() + 7)
    let {startOfWeek:s3, endOfWeek:e3} = this.getStartAndEndDateOfWeek(new Date())
    s3.setDate(s3.getDate() + 14)
    e3.setDate(e3.getDate() + 14)
    this.setState({
      weeks: [...this.state.weeks, {
        startDate: s1,
        endDate: e1,
        Saturday: [],
        Sunday: [],
        Monday: [],
        Tuesday: [],
        Wednesday: [],
        Thursday: [],
        Friday: [],
      },{
        startDate: s2,
        endDate: e2,
        Saturday: [],
        Sunday: [],
        Monday: [],
        Tuesday: [],
        Wednesday: [],
        Thursday: [],
        Friday: [],
      },{
        startDate: s3,
        endDate: e3,
        Saturday: [],
        Sunday: [],
        Monday: [],
        Tuesday: [],
        Wednesday: [],
        Thursday: [],
        Friday: [],
      }],
      serverNotification:[
        {id: 1, context:"Hello", timeout: 10}, 
        {id: 2, context:"Hello", timeout: 20},
        {id: 3, context:"Hello", timeout: 30},
        {id: 4, context:"Hello", timeout: 40},
        {id: 5, context:"Hello", timeout: 50},
        {id: 6, context:"Hello", timeout: 60},
        {id: 7, context:"Hello", timeout: 70}
      ]
    })
    AsyncStorage.getItem("theme", (err, result) => {
      if(err){
        this.setState({theme: true})
        return
      }
      if(result === null || result === undefined){
        this.setState({theme: true})
        return
      }
      this.setState({theme: JSON.parse(result)})
    })
    AsyncStorage.getItem("first", (err, result) => {
      if(err){
        this.setState({step: stages.Tour})
        return
      }
      if(result === null || result === undefined){
        this.setState({step: stages.Tour})
        return

      }
      AsyncStorage.getItem("token", (err, result) => {
        if(err){
          this.setState({step: initialStep})
          return
        }
        if(result === null || result === undefined){
          this.setState({step: initialStep})
          return
        }
        this.setState({token: result}, this.getPlans)
      })
    })
    this.connectivityTimer = setInterval(this.checkConnectivity, 1000)
    PushNotification.configure({onNotification: (notification) => console.log(notification)})
  }

  _onLongPressButton = () => {
    Alert.alert('You long-pressed the button!')
  }

  updatePrefix = (text) => {
    this.setState({prefix: text})
  }

  updateFirst = (text) => {
    this.setState({first: text})
  }

  updateLast = (text) => {
    this.setState({last: text})
  }

  updateMiddle = (text) => {
    this.setState({middle: text})
  }

  updateEmail = (text) => {
    this.setState({email: text})
  }

  updateUsername = (text) => {
    this.setState({username: text})
  }

  updatePassword = (text) => {
    this.setState({password: text})
  }
  
  getPlans = () => {
    if(this.state.online)
      this.setState({step: 2,}, () => fetch(this.state.server_path + '/plan/all?token=' + this.state.token)
      .then((response) => {
        try{
          return Promise.all([response.status, response.json()])
        }
        catch(err){
          this.setState({loadingMessage: "There Is A Network Error"})
          return undefined
        }
      })
      .then(([status, responseJson]) => {
        if(status === 200){
          let newArray = []
          for(i in responseJson.plans)
            newArray.push({
              id: responseJson.plans[i].id,
              name: responseJson.plans[i].name,
              link: responseJson.plans[i].share_link
            })
          AsyncStorage.setItem('plans', JSON.stringify(newArray))
          this.setState({
            step: 3,
            plans: newArray
          })
        }
        else if (status === 404){
          Alert.alert("Get Plans Failed", "You Don't Have Any Plan")
          AsyncStorage.setItem('plans', JSON.stringify([]))
          this.setState({plans: []}, () => this.setState({step: 3}))
        }
        else if (status === 400){
          Alert.alert("App Internal Error", "Please Test Below Option:\n1) Close The App And Reopen it.(Make Sure That The Closed)\n2)If This Doesn't Work, Clear Cache And If That Doesn't Work Too, Clear Data The App.\n3)If None of Above Workd, Contact Support.")
          AsyncStorage.setItem('plans', JSON.stringify([]))
          this.setState({plans: []}, () => this.setState({step: 3}))
        }
        else if (status === 500){
          Alert.alert("Internal Server Error", "Please Retry In A Minute.")
          AsyncStorage.setItem('plans', JSON.stringify([]))
          this.setState({plans: []}, () => this.setState({step: 3}))
        }
      })
      .catch((error) => {
        this.setState({step: 2, loadingMessage: "There Is A Network Error", online: false,}, this.getPlans)
      }))
    else
      AsyncStorage.getItem('plans').then((data) => this.setState({step: 3, plans: JSON.parse(data)})).catch((err) => console.error(err))
  }
  
  updateGUI = () => {
    let newWeeks = []
    for(let week of this.state.weeks){
      week.Saturday = []
      week.Sunday = []
      week.Monday = []
      week.Thursday = []
      week.Wednesday = []
      week.Tuesday = []
      week.Friday = []
      for(let activity of this.state.activities){
        if(activity.show)
          if(this.checkDateExpiration(week.startDate, week.endDate, activity.date.start, activity.date.end)){
            let activityStartDate = new Date(activity.date.start)
            let activityEndDate = new Date(activity.date.end)
            let newStart = new Date()
            newStart.setUTCHours(...activity.time.start.split(':'))
            let newEnd = new Date()
            newEnd.setUTCHours(...activity.time.end.split(':'))
            switch(activity.period.repeat){
              case 'N':
                if(week.startDate < activityStartDate && week.endDate > activityStartDate){
                  switch(activityStartDate.getDate() - week.startDate.getDate()){
                    case 0: week.Saturday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}});break;
                    case 1: week.Sunday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}});break;
                    case 2: week.Monday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}});break;
                    case 3: week.Tuesday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}});break;
                    case 4: week.Wednesday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}});break;
                    case 5: week.Thursday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}});break;
                    case 6: week.Friday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}});break;
                    default: console.warn("No One!");break;
                  }
                }
                break;
              case 'D':
                let eachDay = new Date(week.startDate)
                if(activityStartDate <= eachDay && eachDay <= activityEndDate)
                  week.Saturday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}})
                eachDay.setDate(eachDay.getDate() + 1)
                if(activityStartDate <= eachDay && eachDay <= activityEndDate)
                  week.Sunday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}})
                eachDay.setDate(eachDay.getDate() + 1)
                if(activityStartDate <= eachDay && eachDay <= activityEndDate)
                  week.Monday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}})
                eachDay.setDate(eachDay.getDate() + 1)
                if(activityStartDate <= eachDay && eachDay <= activityEndDate)
                  week.Tuesday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}})
                eachDay.setDate(eachDay.getDate() + 1)
                if(activityStartDate <= eachDay && eachDay <= activityEndDate)
                  week.Wednesday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}})
                eachDay.setDate(eachDay.getDate() + 1)
                if(activityStartDate <= eachDay && eachDay <= activityEndDate)
                  week.Thursday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}})
                eachDay.setDate(eachDay.getDate() + 1)
                if(activityStartDate <= eachDay && eachDay <= activityEndDate)
                  week.Friday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}})
                eachDay.setDate(eachDay.getDate() + 1)
                break;
              case 'W':
                let day = new Date(week.startDate)
                day.setDate(day.getDate() + (activity.period.position - 1))
                if(activityStartDate <= day && day < activityEndDate){
                  switch(activity.period.position - 1/*  activityStartDate.getDay() */){
                    case 0: week.Saturday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}});break;
                    case 1: week.Sunday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}});break;
                    case 2: week.Monday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}});break;
                    case 3: week.Tuesday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}});break;
                    case 4: week.Wednesday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}});break;
                    case 5: week.Thursday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}});break;
                    case 6: week.Friday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}});break;
                    default: console.warn("No One!");break;
                  }
                }
                break;
              case 'M':
                if(week.startDate.getDate() <= (activity.period.position) && (activity.period.position) < week.endDate.getDate()){
                  let day = new Date(week.startDate)
                  day.setDate(activity.period.position)
                  if(activityStartDate <= day && day < activityEndDate){
                    switch((activity.period.position) - week.startDate.getDate()){
                      case 0: week.Saturday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}});break;
                      case 1: week.Sunday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}});break;
                      case 2: week.Monday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}});break;
                      case 3: week.Tuesday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}});break;
                      case 4: week.Wednesday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}});break;
                      case 5: week.Thursday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}});break;
                      case 6: week.Friday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}});break;
                      default: console.warn("No One!");break;
                    }
                  }
                }
                break;
              case 'A':
                if(week.startDate.getDOY() <= activity.period.position && activity.period.position < week.endDate.getDOY()){
                  let day = new Date(week.startDate.getFullYear(), 0)
                  day.setDate(activity.period.position)
                  if(activityStartDate <= day && day < activityEndDate){
                    switch(activity.period.position - week.startDate.getDOY()){
                      case 0: week.Saturday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}});break;
                      case 1: week.Sunday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}});break;
                      case 2: week.Monday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}});break;
                      case 3: week.Tuesday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}});break;
                      case 4: week.Wednesday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}});break;
                      case 5: week.Thursday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}});break;
                      case 6: week.Friday.push({key:activity.id,name:activity.name,detail:activity.detail,type:activity.type.name,time:{start:newStart,end:newEnd}});break;
                      default: console.warn("No One!");break;
                    }
                  }
                }
                break;
              default:
                break;
            }
          }
      }
      newWeeks.push(week)
    }
    this.setState({
      step: 4,
      weeks: newWeeks
    }, this.setAllScrolls)
  }

  onSharePlan = (link) => {
    if(this.state.online)
      this.setState({step: 2,}, () => fetch(link)
        .then((response) => {
          try{
            return Promise.all([response.status, response.json()])
          }
          catch(err){
            console.error(err)
            return undefined
          }
        })
        .then(([status, responseJson]) => {
          if(status === 200){
            console.log(responseJson.activities)
            AsyncStorage.setItem('SharedActivities', JSON.stringify(responseJson.activities))
            this.setState({
              reloadActivityText: "Reload",
              activities: responseJson.activities
            }, this.updateGUI)
          }
          else if (status === 400){
            Alert.alert("App Internal Error", "Please Test Below Option:\n1) Close The App And Reopen it.(Make Sure That The Closed)\n2)If This Doesn't Work, Clear Cache And If That Doesn't Work Too, Clear Data The App.\n3)If None of Above Workd, Contact Support.")
            this.setState({step: stages.Plans})
          }
        })
        .catch((error) => {
          this.setState({step: 2, loadingMessage: "There Is A Network Error", online: false,}, this.getPlanActivities)
        }))
    else
      AsyncStorage.getItem('SharedActivities').then((data) => this.setState({activities: JSON.parse(data), reloadActivityText: "Reload",}, this.updateGUI)).catch((err) => console.error(err))
  }

  getPlanActivities = () => {
    if(this.state.online)
      this.setState({step: 2,}, () => fetch(this.state.server_path + '/activity/all?token=' + this.state.token + '&plan=' + this.state.requested_plan_id)
        .then((response) => {
          try{
            return Promise.all([response.status, response.json()])
          }
          catch(err){
            console.error(err)
            return undefined
          }
        })
        .then(([status, responseJson]) => {
          if(status === 200){
            AsyncStorage.setItem(`activitiesOfPlan${this.state.requested_plan_id}`, JSON.stringify(responseJson.activities))
            this.setState({
              reloadActivityText: "Reload",
              activities: responseJson.activities
            }, this.updateGUI)
          }
          else if (status === 400){
            Alert.alert("App Internal Error", "Please Test Below Option:\n1) Close The App And Reopen it.(Make Sure That The Closed)\n2)If This Doesn't Work, Clear Cache And If That Doesn't Work Too, Clear Data The App.\n3)If None of Above Workd, Contact Support.")
            this.setState({step: stages.Plans})
          }
        })
        .catch((error) => {
          this.setState({step: 2, loadingMessage: "There Is A Network Error", online: false,}, this.getPlanActivities)
        }))
    else
      AsyncStorage.getItem(`activitiesOfPlan${this.state.requested_plan_id}`).then((data) => this.setState({activities: JSON.parse(data), reloadActivityText: "Reload",}, this.updateGUI)).catch((err) => console.error(err))
  }

  setPlanRequesting = (plan_id) => {
    this.setState({
      requested_plan_id: plan_id,
      step: 2,
      plans: [{id: 0, message: "Loading"}]
    }, this.getPlanActivities)
  }

  login = () => {
    this.setState({step:2})
    try{
      return fetch(this.state.server_path + '/login', {
        method: "POST",
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          username: this.state.username,
          password: this.state.password,
        })
      })
      .then((response) => {
        try{
          return Promise.all([response.status, response.json()])
        }
        catch(err){
          this.setState({loadingMessage: "There Is A Network Error"})
          return undefined
        }
      })
      .then(([status, responseJson]) => {
        if(status === 200){
          AsyncStorage.setItem("token", responseJson.token)
          this.setState({
            token: responseJson.token,
          }, this.getPlans)
        }
        else if (status === 404){
          Alert.alert("Login Failed!", "Username or Password are Incorrect")
          this.setState({step: stages.Auth})
        }
        else if (status === 500){
          Alert.alert("Internal Server Error", "Please Retry In A Minute.")
          this.setState({step: stages.Auth})
        }
        else if (status === 400){
          Alert.alert("Incompelete Form", "Please Fill Both Username And Password Fields.")
          this.setState({step: stages.Auth})
        }
      })
      .catch((error) => {
        console.warn(error)
        this.setState({loadingMessage: "There Is A Network Error"})
        return undefined
      })
    }
    catch(err){
      this.setState({loadingMessage: "There Is A Network Error"})
      return undefined
    }
  }

  register = () => {
    this.setState({step:2})
    try{
      return fetch(this.state.server_path + '/register', {
        method: "POST",
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          username: this.state.username,
          password: this.state.password,
          email: this.state.email,
          prefix: this.state.prefix,
          first_name: this.state.first,
          middle_name: this.state.middle,
          last_name: this.state.last
        })
      })
      .then((response) => {
        try{
          const statusCode = response.status
          const data = response.json()
          return Promise.all([statusCode, data])
        }
        catch(err){
          this.setState({loadingMessage: "There Is A Network Error"})
          return undefined
        }
      })
      .then(([status, responseJson]) => {
        if(status === 201){
          Alert.alert("Succeed", "You Have Registered Successfully.\nPlease Login.")
          this.setState({
            register: false,
            step: 1,
          })
        }
        else if(status === 400){
          if(responseJson.message === 'User Already Exist'){
            Alert.alert("User Already Exist", "Please Login If You Already Registered.")
            this.setState({
              register: true,
              step: 1,
            })
          }
          else{
            Alert.alert("Incompelete Form", "Please Fill Both Username And Password Fields.")
            this.setState({step: stages.Auth})
          }
        }
        else{
          Alert.alert("Uregonized Error", "Please Try In A Minute.")
          console.warn(status)
        }
      })
      .catch((error) => {
        this.setState({loadingMessage: "There Is A Network Error"})
        return undefined
      })
    }
    catch(err){
      this.setState({loadingMessage: "There Is A Network Error"})
      return undefined
    }
  }

  logout = () => {
    AsyncStorage.removeItem('token')
    this.setState({step: 1})
  }

  render() {
    let styles = StyleSheet.create({
      getNewSharedPlanContainer:{
        elevation: 10,
        borderRadius: 5,
        flex: 1,
        backgroundColor: this.state.theme ? '#e5e5e5' : '#232323',
      },
      getNewSharedPlanElementcontainer:{
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
      },
      getNewSharedPlanGuideText:{
        textAlign: 'center',
        marginTop: 40,
        marginBottom: 5,
        color: this.state.theme ? 'black' : 'white'
      },
      getNewSharedPlanInput:{
        borderBottomWidth: 1,
        borderBottomColor: this.state.theme ? '#bb86fc' : '#00dbc5',
        textAlign: 'center',
        color: this.state.theme ? 'black' : 'white'
      },
      getNewSharedPlanButtonContainer:{
        borderRadius:2,
        backgroundColor: this.state.theme ? '#bb86fc' : '#00dbc5', /* #2196F3 */ 
        justifyContent: 'center',
        alignItems: 'center',
      },
      getNewSharedPlanButtonToucahbleArea:{
        margin: 0,
        justifyContent: 'center',
        alignItems: 'center',
      },
      getNewSharedPlanButtonContext:{
        color: 'black'
      },
      getNewSharedPlanTitleContext:{
        marginTop: 20,
        paddingBottom: 15,
        borderBottomColor: this.state.theme ? 'black' : 'white',
        borderBottomWidth: 1,
        marginBottom: 4,
        width: '100%',
        fontSize: 20,
        color: this.state.theme ? 'black' : 'lightgray',
        textAlign: 'center'
      },
      uploadText:{
        color: '#b28bec',
        textAlign: 'center',
        marginTop: 10,
      },
      progressBarContainer:{
        position: 'absolute',
        top: 0,
        right: 0,
      },
      loginContainer:{
        paddingHorizontal: 10,
        justifyContent: 'space-around',
        flex: 1,
        backgroundColor: '#121212',
        width: screenWidth - 20,
        height: screenHeight - 500,
        borderRadius: 10,
      },
      inputStyle:{
        borderColor: this.state.theme ? '#bb86fc' : '#00dbc5', 
        borderWidth: 0, 
        borderBottomWidth:2, 
        color:'#aaaaaa', 
        textAlign: 'center',
        margin: 10, 
      },
      buttonStyle:{
        height: 50,
        width: 50,
        backgroundColor: this.state.theme ? '#bb86fc' : '#00dbc5', /* #2196F3 */ 
        borderRadius: 25,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      },
      buttonText: {
        fontSize: 20, 
        fontFamily: "Material-Design-Iconic-Font", 
        color: 'black',
      },
      button: { 
        marginTop: 2,
        marginBottom: 2,
        alignItems: 'center',
        color: this.state.theme ? '#e5e5e5' : '#000000',
        backgroundColor: this.state.theme ? '#bb86fc' : '#00dbc5', /* #2196F3 */
        borderRadius: 10
      },
      addElementsButtonTouchableArea:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: this.state.theme ? '#bb86fc' : '#00dbc5',
        borderRadius: 5,
      },
      addButtonTouchableArea:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: this.state.theme ? '#bb86fc' : '#00dbc5',
        borderRadius: 25
      },
      addElementsButtonContainer: {
        height: 50,
        width: 60,
        position: 'absolute',
        right: 20,
        bottom: 40,
        justifyContent: 'center',
      },
      addButtonContainer: {
        height: 50,
        width: 50,
        position: 'absolute',
        right: 20,
        bottom: 20,
        justifyContent: 'center',
      },
      addElementsButton:{
        textAlign: 'center',
        color: 'black',
      },
      addButton:{
        textAlign: 'center',
        color: 'black',
        fontSize: 20, 
        fontFamily: "Material-Design-Iconic-Font",
      },
      buttonContainer:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      },
      container:{
        height: screenHeight,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: this.state.theme ? '#e5e5e5' : '#000000',
      },
      newActivityDialogContainer:{
        flex: 1,
        backgroundColor: this.state.theme ? '#e5e5e5' : '#000000',
        width: screenWidth,
      },
      containerFirst: {
        flex: 1,
        backgroundColor: this.state.theme ? '#e5e5e5' : '#000000',
      },
      containerSecond: {
        position: 'absolute',
        right: 10,
        bottom: 10,
        justifyContent: 'center',
      },
      welcome: {
        color: '#aaaaaa',
        fontSize: 30,
        textAlign: 'center',
        margin: 40,
        marginBottom: 60,
      },
      instructions: {
        textAlign: 'center',
        color: '#aaaaaa',
        marginBottom: 5,
      },
      buttonContainer: {
        margin: 20
      },
      alternativeLayoutButtonContainer: {
        margin: 20,
        flexDirection: 'row',
        justifyContent: 'space-between'
      },
      buttonText: {
        fontSize: 30, 
        fontFamily: "Material-Design-Iconic-Font",
        color: 'black',
        padding: 10,
      },
      activitiesPresenter:{
        flexDirection: 'row',
        width: rows.width*24 + daysOfWeekIndicators.width, 
        height: (screenHeight - DimensionsError - 65)/7, 
        color: this.state.theme ? 'white' : 'white', 
        borderWidth:1, 
        backgroundColor: '#b28bec10', 
        borderColor: '#808080',
      },
      activitiesPresenterIndicators:{
        width: rows.width*24 + daysOfWeekIndicators.width, 
        height: 65,//(screenHeight - DimensionsError)/8, 
        color: this.state.theme ? 'white' : 'white', 
        backgroundColor: this.state.theme ? '#6200eeff' : '#121212'
      },
      activitiesPresenterNodesContainer:{
        flex: 1,
        width: rows.width - rows.horizentalmargin, 
        marginLeft: rows.horizentalmargin,
        marginRight: rows.horizentalmargin,
        alignContent: 'center' , 
        justifyContent: 'center',
        borderRightWidth:1, 
        borderColor: this.state.theme ? '#bb86fc50' : '#808080',
        backgroundColor: this.state.theme ? '#ffffff00' : '#b28bec30',
      },
      activitiesPresenterNodes:{
        color: this.state.theme ? 'white' : 'white',
        paddingTop: daysOfWeekIndicators.verticalmargin,
        textAlign: "center"
      },
      activitiesPresenterContainer:{
        width: screenWidth, 
        height: screenHeight - DimensionsError, 
        marginLeft: daysOfWeekIndicators.width
      },
      reloadButton:{
        textAlign: 'center',
        width: daysOfWeekIndicators.width - 2 * (daysOfWeekIndicators.diffrenceBetweenButtonAndText/2),
        color: this.state.theme ? 'white' : 'white',
        fontSize: 20, 
        fontFamily: "Material-Design-Iconic-Font",
      },
      reloadButtonTouchable:{
        backgroundColor: this.state.theme ? '#ffffff00' : '#121212',
      },
      reloadButtonContainer:{
        width: daysOfWeekIndicators.width, 
        flex: 1,
        height: 66,//((screenHeight - DimensionsError)/8) - 2*daysOfWeekIndicators.verticalmargin, 
        marginTop: daysOfWeekIndicators.verticalmargin,
        marginBottom: daysOfWeekIndicators.verticalmargin,
        alignContent: 'center' , 
        justifyContent: 'center',
        backgroundColor: this.state.theme ? '#6200eeff' : '#b28bec30',
        borderRightWidth: 1,
        borderBottomWidth: 1,
        borderColor: this.state.theme ? '#bb86fc50' : '#808080',
      },
      daysOfWeekIndicators:{
        width: daysOfWeekIndicators.width, 
        paddingTop: ((screenHeight - DimensionsError)/8)*3/8,
        textAlign: 'center',
        height: ((screenHeight - DimensionsError-66)/7) - 2*daysOfWeekIndicators.verticalmargin, 
        marginTop: daysOfWeekIndicators.verticalmargin,
        marginBottom: daysOfWeekIndicators.verticalmargin,
        color: this.state.theme ? 'black' : '#efefef', 
        borderBottomWidth: 1, 
        borderRightWidth: 1,
        borderColor: '#808080',
      },
      daysOfWeekIndicatorsOverlay:{
        backgroundColor: "#b28bec20"
      },
      daysOfWeekIndicatorsContentContainerStyle:{
        alignContent: 'center' , 
        justifyContent: 'center',
        
      },
      daysOfWeekIndicatorsContainer:{
        position: 'absolute',
        top: 0,
        left: 0,
        backgroundColor: this.state.theme ? '#e5e5e5' : '#000000'
      },
      newActivityContainer:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: this.state.theme ? '#e5e5e5' : '#000000',
        width: screenWidth,
      },
      newActivityInput:{
        backgroundColor:'#1e1e1e', 
        color:'#aaaaaa', 
        width: screenWidth - 20,
        marginLeft: 10, 
        marginRight: 10, 
        marginBottom: 10, 
        marginTop: 10,
      },
      newActivityGuideText:{
        backgroundColor:'#FFFFFF00',
        textAlign: 'center',
        color: '#aaaaaa',
        width: screenWidth - 20,
        marginLeft: 10, 
        marginRight: 10, 
        marginBottom: 10, 
        marginTop: 10,
      },
      newActivityCheckBoxText:{
        textAlign: 'center',
        color: '#aaaaaa',
        marginLeft: 10, 
        marginRight: 10, 
        marginBottom: 10, 
        marginTop: 7,
      },
      addDialogBackButton:{
        position: 'absolute',
        left: 10,
        bottom: 10,
        flex: 0,
        justifyContent: 'center',
      }
    });

    return (
      <View style={styles.container}>
        {this.state.step === stages.Tour ? <Tour onTourCompelete={this.onTourCompelete}/> : null}

        {/****************Login/Register Page********************/}
        {this.state.step === stages.Auth ?  <ScrollView style={styles.containerFirst}>
          <Text style={styles.welcome} onLongPress={() => {this.setState({theme: !this.state.theme})}}>PLAN VIEWER</Text>
          {this.state.register 
          ? 
            <Register 
              theme={this.state.theme}
              screenWidth={screenWidth} 
              screenHeight={screenHeight} 
              prefix={this.state.prefix}l
              updatePrefix={this.updatePrefix} 
              first={this.state.first}
              updateFirst={this.updateFirst} 
              last={this.state.last}
              updateLast={this.updateLast} 
              middle={this.state.middle}
              updateMiddle={this.updateMiddle} 
              username={this.state.username}
              updateUsername={this.updateUsername} 
              password={this.state.password}
              updatePassword={this.updatePassword} 
              email={this.state.email}
              updateEmail={this.updateEmail}  
            /> 
          : 
            <Login 
              theme={this.state.theme}
              screenWidth={screenWidth} 
              screenHeight={screenHeight} 
              updateUsername={this.updateUsername} 
              username={this.state.username}
              updatePassword={this.updatePassword} 
              password={this.state.password}
            />
          }
        </ScrollView>:null}
        {this.state.step === stages.Auth ? (<View style={{position: 'absolute', left: 10, bottom: 10, flex: 0, justifyContent: 'center',}}>
          <View style={styles.buttonContainer}>
            <TouchableNativeFeedback
                onPress={() => this.setState({register: !this.state.register,})}
                background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
              <View style={styles.buttonStyle}>
                <Text style={styles.buttonText}>{!this.state.register ? '\uf1ff' : '\uf207'}</Text>
              </View>
            </TouchableNativeFeedback>
          </View>
        </View>):null}
        {this.state.step === stages.Auth ? (<View style={styles.containerSecond}>
          <View style={styles.buttonContainer}>
            <TouchableNativeFeedback
                onPress={() => (this.state.register) ? this.register() : this.login()}
                background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
              <View style={styles.buttonStyle}>
                <Text style={styles.buttonText}>{"\uf2ee"}</Text>
              </View>
            </TouchableNativeFeedback>
          </View>
        </View>):null}

        {/*********************Loading Page***********************/}
        {[stages.Loading                  ].includes(this.state.step) ? (<CircleSnail size={100} thickness={5} width={100} height={100} color={'#b28bec'} indeterminate/>) : null}
        {[stages.Uploading                ].includes(this.state.step) ? (<Circle size={100} borderColor={'#ffff00'} thickness={5} width={100} indeterminate={this.state.uploadProgress === 1} height={100} progress={this.state.uploadProgress} color={'#00dbc5'} />) : null}
        {[stages.Uploading                ].includes(this.state.step) ? <Text style={styles.uploadText}>{this.state.uploadProgress !== 1 ? "Uploading" : "Please Wait"}...</Text> : null}

        {/*********************Settings Page***********************/}
        {![stages.Loading, stages.Auth    ].includes(this.state.step) ? (<Modal isVisible={this.state.settings} onRequestClose={() => {this.setState({settings: false})}} >
          <View style={styles.getNewSharedPlanContainer}>
            <View style={styles.getNewSharedPlanElementcontainer}>
              <Text style={styles.getNewSharedPlanTitleContext}>Settings</Text>
              <ButtonGroup 
                onPress={(newIndex) => this.setState({selectedServerType: newIndex}, () => this.setState({server_path: global_server_path}))} 
                buttonStyle={styles.getNewSharedPlanButtonToucahbleArea} 
                selectedIndex={this.state.selectedServerType} 
                selectedButtonStyle={styles.getNewSharedPlanButtonContainer}
                buttons={[
                  {
                    element: () => <Text style={styles.getNewSharedPlanButtonContext}>
                      Global
                      </Text>
                  },{
                    element: () => <Text style={styles.getNewSharedPlanButtonContext}>
                      Other(HTTP)
                      </Text>
                  },{
                    element: () => <Text style={styles.getNewSharedPlanButtonContext}>
                      Other(HTTPS)
                      </Text>
                  }]} 
                containerStyle={{height: 30, width: '100%', margin: 0}}/>
              {this.state.selectedServerType ? <TextInput 
                style={styles.getNewSharedPlanInput} 
                value={this.state.server_path}
                placeholder={"Insert New Server Path"} 
                placeholderTextColor={'grey'} 
                onChangeText={(newPath) => this.setState({server_path: newPath})}/> : null}
            </View>
          </View>
        </Modal>): null}

        {/*********************Main Menu**************************/}
        {this.state.step === stages.Plans ? <PlanManager
          onSharePlan={this.onSharePlan}
          theme={this.state.theme}
          online={this.state.online}
          sideMenuContent={this.state.sideMenuContent}
          orientation={this.state.orientation}
          addNewPlan={() => this.setState({step: stages.AddPlan, addPlanPreiviousStep: stages.Plans})}
          plans={this.state.plans}
          setPlanRequesting={this.setPlanRequesting}
          server_path={this.state.server_path}
          screenHeight={screenHeight}
          screenWidth={screenWidth}
          shareForAll={this.shareForAll}
          deletePlan={this.deletePlan}
        />:null}
        {this.state.step === stages.Plans && this.state.online ? <View style={styles.addButtonContainer}>
            <TouchableNativeFeedback
                onLongPress={() => PushNotification.localNotificationSchedule({
                    message: "My Notification Message",
                    date: new Date(Date.now() + (5 * 1000))
                  })
                }
                onPress={() => this.setState({step: stages.AddPlan, addPlanPreiviousStep: stages.Plans})}
                background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
              <View style={styles.addButtonTouchableArea}>
                <Text style={styles.addButton}>{'\uf278'}</Text>
              </View>
            </TouchableNativeFeedback>
        </View>: null}

        {/***************Notification Manger*********************/}
        {this.state.step === stages.ManageNotifications ? <NotificationManager 
          theme={this.state.theme}
          online={this.state.online}
          sideMenuContent={this.state.sideMenuContent}
          screenWidth={screenWidth} 
          screenHeight={screenHeight} 
          notifications={this.state.serverNotification} 
          onBack={() => this.getPlans()}
          onAdd={this.addServerNotification}
          onDeleteNotification={this.deleteServerNotification}/> :null}

        {/*******************News Manger**************************/}
        {this.state.step === stages.ManageNews ? <NewsManager 
          theme={this.state.theme}
          online={this.state.online}
          addPicture={this.addPicture}
          setTimeout={this.setNewsTimeout}
          addNews={this.addNews}
          onDeleteNews={this.deleteNews}
          news={this.state.news}
          sideMenuContent={this.state.sideMenuContent}
          screenHeight={screenHeight}
          screenWidth={screenWidth}/> : null}

        {/********************Activities*************************/}
        {this.state.step === stages.Activities || this.state.step === stages.AddMenu ? (<FlatList style= {{flex: 1}} data={this.state.weeks} keyExtractor={(item, index) => item.startDate.toString()} pagingEnabled renderItem={({item}) => {
          return (
            <ScrollView 
              ref={component => this.scrolls[item.startDate.toString()] = component} 
              horizontal 
              onMomentumScrollEnd={this.setAllScrolls} 
              style={styles.activitiesPresenterContainer}
            >
              <View>
                <FlatList 
                  horizontal 
                  contentContainerStyle={{justifyContent: 'center',}} 
                  style={{...styles.activitiesPresenterIndicators, borderWidth: 0}} 
                  data={this.state.timesOfDays} 
                  keyExtractor={(item) => item.start.toString()} 
                  renderItem={({item}) =>
                  <View style={styles.activitiesPresenterNodesContainer}>
                    <Text style={styles.activitiesPresenterNodes}>{item.start.toString() + ' - ' + item.end}</Text>
                  </View>
                }></FlatList>
                <ActivityRow theme={this.state.theme} online={this.state.online} onActivitylongPress={this.editActivity} style={styles.activitiesPresenter} activityMargin={activityMargin + 1} rowsHeight={rows.height} rowsWidth={rows.width} activities={item.Saturday}/>
                <ActivityRow theme={this.state.theme} online={this.state.online} onActivitylongPress={this.editActivity} style={styles.activitiesPresenter} activityMargin={activityMargin + 1} rowsHeight={rows.height} rowsWidth={rows.width} activities={item.Sunday}/>
                <ActivityRow theme={this.state.theme} online={this.state.online} onActivitylongPress={this.editActivity} style={styles.activitiesPresenter} activityMargin={activityMargin + 1} rowsHeight={rows.height} rowsWidth={rows.width} activities={item.Monday}/>
                <ActivityRow theme={this.state.theme} online={this.state.online} onActivitylongPress={this.editActivity} style={styles.activitiesPresenter} activityMargin={activityMargin + 1} rowsHeight={rows.height} rowsWidth={rows.width} activities={item.Tuesday}/>
                <ActivityRow theme={this.state.theme} online={this.state.online} onActivitylongPress={this.editActivity} style={styles.activitiesPresenter} activityMargin={activityMargin + 1} rowsHeight={rows.height} rowsWidth={rows.width} activities={item.Wednesday}/>
                <ActivityRow theme={this.state.theme} online={this.state.online} onActivitylongPress={this.editActivity} style={styles.activitiesPresenter} activityMargin={activityMargin + 1} rowsHeight={rows.height} rowsWidth={rows.width} activities={item.Thursday}/>
                <ActivityRow theme={this.state.theme} online={this.state.online} onActivitylongPress={this.editActivity} style={styles.activitiesPresenter} activityMargin={activityMargin + 1} rowsHeight={rows.height} rowsWidth={rows.width} activities={item.Friday}/>
              </View>
            </ScrollView>);
        }} onMomentumScrollEnd={this.updateWeekList}>
        </FlatList>):null}
        {this.state.step === stages.Activities || this.state.step === stages.AddMenu ?(<FlatList style={styles.daysOfWeekIndicatorsContainer} data={[{key:""},{key:"Saturday"},{key:"Sunday"},{key:"Monday"},{key:"Tuesday"},{key:"Wednesday"},{key:"Thursday"},{key:"Friday"}]} renderItem={({item}) => {
            if(item.key !== '')
              return <View style={styles.daysOfWeekIndicatorsOverlay}><View style={styles.daysOfWeekIndicators}><Text style={{color: styles.daysOfWeekIndicators.color, textAlign: styles.daysOfWeekIndicators.textAlign}}>{item.key}</Text></View></View>
            else
              return <TouchableNativeFeedback
              style={styles.reloadButtonTouchable}
              onPress={this.getPlans}
              background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
            <View style={styles.reloadButtonContainer}>
              <Text style={styles.reloadButton}>{'\uf2ea'}</Text>
            </View>
          </TouchableNativeFeedback>
          }}></FlatList>):null}
        {(this.state.step === stages.Activities || this.state.step === stages.AddMenu) && this.state.online ? (<View style={styles.addButtonContainer}>
          <TouchableNativeFeedback
              onPress={()=> this.state.step === stages.Activities ? this.setState({step:stages.AddMenu}) : this.setState({step:stages.Activities})}
              background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
              <View style={styles.addButtonTouchableArea}>
                <Text style={styles.addButton}>{this.state.step === stages.Activities ? '\uf278' : '\uf136'}</Text>
            </View>
          </TouchableNativeFeedback></View>):(null)}

        {/*********************Add Menu**************************/}
        
        {this.state.step === stages.AddMenu ? <View style={{...styles.addElementsButtonContainer, bottom:  85, height: 40}}>
          <TouchableNativeFeedback
              onPress={()=> this.updateActivityTypesAndPriorities(stages.addActivity)}
              background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
              <View style={styles.addElementsButtonTouchableArea}>
                <Text style={styles.addElementsButton}>Activity</Text>
            </View>
          </TouchableNativeFeedback>
        </View>:null}
        {this.state.step === stages.AddMenu ? <View style={{...styles.addElementsButtonContainer, bottom: 135, height: 40}}>
          <TouchableNativeFeedback
              onPress={()=> this.updatePlansAndActivityTypesAndPriorities(stages.AddAccess)}
              background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
              <View style={styles.addElementsButtonTouchableArea}>
                <Text style={styles.addElementsButton}>Access</Text>
            </View>
          </TouchableNativeFeedback>
        </View>:null}

        {/*********************New Plan**************************/}
        {this.state.step === stages.AddPlan ? <NewPlan 
          theme={this.state.theme}
          onBack={() => this.setState({step: this.state.addPlanPreiviousStep})}
          onFinish={this.addPlan} 
          screenWidth={screenWidth}
          screenHeight={screenHeight}
        /> : null}

        {/********************New Activity***********************/}
        {this.state.step === stages.addActivity ? <NewActivity 
          theme={this.state.theme}
          onBack={() => this.setState({step: stages.Activities})}
          onFinish={this.newAddActivity}//addActivity} 
          activityTypes={this.state.activityTypes} 
          activityPriorities={this.state.activityPriorities} 
          screenWidth={screenWidth}
          screenHeight={screenHeight}
        /> : null}

        {/********************New Access*************************/}
        {this.state.step === stages.AddAccess ? <NewAccess
          theme={this.state.theme}
          onBack={() => this.setState({step: stages.Activities})}
          onFinish={this.addAccess} 
          onSuccess={this.getPlans}
          backButtonContainerStyle={styles.addDialogBackButton}
          activityTypes={this.state.activityTypes} 
          activityPriorities={this.state.activityPriorities} 
          plans={this.state.plans}
          activities={this.state.activities}
          screenWidth={screenWidth}
          screenHeight={screenHeight}
        /> : null}
        {this.state.step !== stages.Tour ? <View style={styles.progressBarContainer}>
          <Bar width={screenWidth} height={2} color={this.state.online?(this.state.step === stages.Uploading ? '#ffff00' : '#00ff00'):'red'} unfilledColor={this.state.theme ? '#e5e5e5' : '#000000'} indeterminate={!this.state.online || this.state.step === stages.Uploading} progress={1} borderRadius={0} borderWidth={0}/>
        </View> : null}
      </View>
    );
  }
}

