import React, {Component} from 'react'
import {View, Text, ScrollView, TouchableNativeFeedback, Dimensions, Platform} from 'react-native'

export class SelectDays extends Component{
    constructor(props){
        super(props)
        this.state={
            selectedDays: new Array(),
        }
    }

    render(){
        let styles ={
            container:{

            },
            scrollArea:{

            },
            button:{
                container:{
                    width: this.props.height,
                    height: this.props.height,
                    borderRadius: this.props.height/2,
                },
                background:{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    width: this.props.height,
                    height: this.props.height,
                    borderRadius: this.props.height/2,
                    borderColor: this.props.foregroundColor,
                    borderWidth: 1,
                },
            }
        }
        return (<View style={{height: this.props.height, width: this.props.width, backgroundColor: '#00000000'}}>
            <ScrollView horizontal style={{flex: 1, backgroundColor: '#88888888ff'}}>
                {this.props.days.map((day) =><View style={styles.button.container}>
                    <TouchableNativeFeedback
                        onPress={() => {
                            let selectedDays = this.state.selectedDays
                            if(selectedDays.includes(day.number))
                                selectedDays.splice(selectedDays.indexOf(day.number), 1)
                            else
                                selectedDays.push(day.number)
                            this.setState({selectedDays: selectedDays}, () => {this.props.onUpdate(this.state.selectedDays)})
                        }}
                        background={Platform.OS === 'android' ? TouchableNativeFeedback.Ripple(this.props.backgroundColor, false) : ''}>
                        <View style={{...styles.button.background, backgroundColor: this.state.selectedDays.includes(day.number) ? this.props.clickedColor : this.props.foregroundColor}}>
                            <Text style={{color: this.state.selectedDays.includes(day.number) ? this.props.textClickedColor : this.props.textUnclickedColor}}>{day.name}</Text>
                        </View>
                    </TouchableNativeFeedback>
                </View>)}
            </ScrollView>
        </View>)
    }
}