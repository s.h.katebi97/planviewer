import React, {Component} from 'react'
import {View, Platform, Clipboard, Text, ToastAndroid, Dimensions, TouchableNativeFeedback, Alert, FlatList, Button, TextInput} from 'react-native'
import { SideMenu } from './SideMenu'
import { Theme } from './shared.js'
import Modal from "react-native-modal"

let screenHeight = Dimensions.get('window').height
let screenWidth = Dimensions.get('window').width

export class PlanManager extends Component{
    constructor(props){
        super(props)
        this.state={
          sideMenu: false,
          getSharedPlanLink: false,
          sharedPlanLink: '',
        }
    }

    sideMenu = () => this.setState({sideMenu: !this.state.sideMenu})

    addSharedPlan = () => {
      this.setState({getSharedPlanLink: true})
    }

    addSharedPlanDone = () => {
      this.setState({getSharedPlanLink: false}, () => {this.props.onSharePlan(this.state.sharedPlanLink)})
    }

    render(){
      let styles = {
        getNewSharedPlan:{
          container:{
            borderRadius: 10,
            height: 120,
            width: screenWidth - 50,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: this.props.theme ? '#e5e5e5' : '#232323',
          },
          elementcontainer:{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          },
          guideText:{
            textAlign: 'center',
            marginTop: 40,
            marginBottom: 5,
            color: this.props.theme ? 'black' : 'white'
          },
          input:{
            textAlign: 'center',
            marginBottom: 10,
            color: this.props.theme ? 'black' : 'white'
          },
          button:{
            container:{
              height: 50,
              width: 50,
              borderRadius:25,
              backgroundColor: this.props.theme ? '#bb86fc' : '#00dbc5', /* #2196F3 */ 
              justifyContent: 'center',
              alignItems: 'center',
            },
            toucahbleArea:{
              height: 50,
              width: 50,
              justifyContent: 'center',
              alignItems: 'center',
            },
            context:{
              fontSize: 25,
              fontFamily: "Material-Design-Iconic-Font",
              color: 'black'
            },
          }
        },
        containerFirst: {
            width: screenWidth - 20,
            marginHorizontal: 10,
            flex: 1,
            marginTop: 2,
        },
        addPlan:{
          container:{
            width: screenWidth - 20,
            backgroundColor: '#121221a0',
            flex: 1, 
            justifyContent: 'center', 
            alignItems: 'center', 
            marginHorizontal: 10,
          },
          context:{
            fontSize: 35,
            textAlign: 'center',
            fontFamily: "Material-Design-Iconic-Font",
            color: 'white',
          },
        },
        overlay:{
          backgroundColor: this.props.theme ? '#6200eeff' : '#b28bec30',
        },
        buttonContainer:{
            flex: 1,
            width: screenWidth - 26,
            borderRadius: 5,
            margin: 3,
            backgroundColor: this.props.theme ? '#ffffff' : '#decef740',
            elevation: 3,
        },
        button: { 
            borderRadius: 5,
            padding: 10,
            backgroundColor: this.props.theme ? '#3700b300' :'#b28bec19', /* #2196F3 */
        },
        buttonText: {
            padding: 10,
            color: this.props.theme ? 'black' : 'white'
        },
        instructions: {
            color: '#aaaaaa',
            marginTop: 20,
        },
        container:{
            flex: 1,
            backgroundColor: this.props.theme ? '#e5e5e5' : '#000000',
        },
        header:{
          overlay:{
            backgroundColor: this.props.theme ? '#6200eeff' : '#b28bec30',
            flexDirection: 'row',
            width: screenWidth,
            justifyContent: 'center', 
            alignItems: 'center', 
          },
          container:{
            backgroundColor: this.props.theme ? '#ededed' : '#121212', //'#b28bec',
            elevation: 5,
          },
          title:{
            flex: 10,
            color: 'white',
            fontSize: 30,
            marginLeft: 10,
            marginBottom: 5,
            paddingVertical: 10,
          },
          connection:{
            flex: 10,
            color: 'white',
            fontSize: 30,
            marginLeft: 10,
            marginBottom: 5,
            paddingVertical: 10,
          },
          button:{
            container:{
              marginLeft: 10,
              flex: 1, 
              justifyContent: 'center', 
              alignItems: 'center', 
            },
            touchableArea:{
              height: 65,
              width: 65,
              justifyContent: 'center', 
              alignItems: 'center', 
            },
            context:{
              fontSize: 35,
              fontFamily: "Material-Design-Iconic-Font",
              color: 'white',
            }
          },
          addSharedPlan: {
            buttonContainer:{
              flex: 1,
              height: 50,
              borderRadius: 25,
              margin: 3,
              justifyContent: 'center', 
              alignItems: 'center', 
            },
            touchableArea:{
              height: 65,
              width: 65,
              justifyContent: 'center', 
              alignItems: 'center', 
            },
            buttonText: {
              fontSize: 30,
              fontFamily: "Material-Design-Iconic-Font",
              color: 'white'
            },
          }
        },
      }
      return(
        <View style={styles.container}>
          <Modal isVisible={this.state.getSharedPlanLink} onRequestClose={() => {this.setState({getSharedPlanLink: false})}} >
            <View style={styles.getNewSharedPlan.container}>
              <View style={styles.getNewSharedPlan.elementcontainer}>
                <Text style={styles.getNewSharedPlan.guideText}>Paste Another User Shared Plan</Text>
                <TextInput style={styles.getNewSharedPlan.input} placeholder={"Paste Your Link Here"} placeholderTextColor={'grey'} onChangeText={(newText) => {this.setState({sharedPlanLink: newText})}}/>
                <View style={styles.getNewSharedPlan.button.container}>
                  <TouchableNativeFeedback
                    onPress={this.addSharedPlanDone}
                    background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
                      <View style={styles.getNewSharedPlan.button.toucahbleArea}>
                        <Text style={styles.getNewSharedPlan.button.context}>{"\uf2ee"}</Text>
                      </View>
                  </TouchableNativeFeedback>
                </View>
              </View>
            </View>
          </Modal>
          <View style={styles.header.container}>
            <View style={styles.header.overlay}>
              <View style={styles.header.button.container}>
                <TouchableNativeFeedback
                  onPress={this.sideMenu}
                  background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
                    <View style={styles.header.button.touchableArea}>
                      <Text style={styles.header.button.context}>{"\uf197"}</Text>
                    </View>
                </TouchableNativeFeedback>
              </View>
              <Text style={styles.header.title}>Plans</Text>
              <View style={styles.header.addSharedPlan.buttonContainer}>
                <TouchableNativeFeedback
                  onPress={this.addSharedPlan}
                    background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
                  <View style={styles.header.addSharedPlan.touchableArea}>
                    <Text style={styles.header.addSharedPlan.buttonText}>{'\uf35b'}</Text>
                  </View>
                </TouchableNativeFeedback>
              </View>
            </View>
          </View>
          <View style={{position: "absolute", top: 70, left: 0}}>
            <FlatList horizontal={this.props.orientation} extraData={this.props.online} style={styles.containerFirst} data={this.props.plans} keyExtractor={(item) => item.id.toString()} renderItem={({item}) => {
              return(
                <View style={styles.buttonContainer}>
                  <TouchableNativeFeedback
                      onLongPress={() => {
                          if(this.props.online) Alert.alert("Plan Details",
                            `Plan ${item.id}`,
                            [
                              {text: `Copy Link`, onPress: () => {
                                Clipboard.setString(`${this.props.server_path}/activity/all_activities?link=${item.link}`)
                                ToastAndroid.showWithGravityAndOffset(
                                  'Link Copied To Clipboard Successfully.',
                                  ToastAndroid.LONG,
                                  ToastAndroid.CENTER,
                                  25,
                                  50,
                                );
                                }},
                              {text: `Share To All`, onPress: () => {this.props.shareForAll(item.link)}},
                              {text: `Delete`, onPress: () => {this.props.deletePlan(item.link)}},
                            ])
                        }
                      }
                      onPress={() => this.props.setPlanRequesting(item.id)}
                      background={Platform.OS === 'android' ? TouchableNativeFeedback.Ripple(styles.container.backgroundColor, false) : ''}>
                    <View style={styles.button}>
                      <Text style={styles.buttonText}>{item.name}</Text>
                    </View>
                  </TouchableNativeFeedback>
                </View>
              )
            }}/>
          </View>
          {this.state.sideMenu === true ?(<SideMenu online={this.props.online} orientation={this.props.orientation} screenWidth={screenWidth} screenHeight={screenHeight} sideMenuContent={this.props.sideMenuContent} theme={this.props.theme} />):null}
        </View>
      )
    }
}
