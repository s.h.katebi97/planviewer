// False For Dark And True For Light
export var Theme = new class theme {
    constructor(){
        let _theme = true
        this.set = (newTheme) => _theme = newTheme
        this.get = () => _theme
    }
}
