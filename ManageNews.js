import React, {Component} from 'react'
import {View, Platform, Text, TextInput, TouchableNativeFeedback, FlatList, Image, Alert, ScrollView, Dimensions, TouchableWithoutFeedback} from 'react-native'
import DocumentPicker from 'react-native-document-picker';
import { SideMenu } from './SideMenu'

let screenHeight = Dimensions.get('window').height
let screenWidth = Dimensions.get('window').width

export class NewsManager extends Component{
    constructor(props){
        super(props)
        this.state={
            sideMenu: false,
            step: 1,
            name: "",
            timeout: 0,
            id: 0,
            show: true,
        }
    }
  
    sideMenu = () => this.setState({sideMenu: !this.state.sideMenu})
    
    nextStep = () => {
        if(this.state.step <= 3)
            switch(this.state.step){
                case 1: 
                    this.setState({step: this.state.step + 1})
                    break
                case 2:
                    this.props.addNews(this.state.name, (err, response) => this.setState({step: 3, id: response.id}))
                    break
                case 3:
                    this.props.setTimeout(this.state.id, this.state.timeout, (err, response) => {
                        DocumentPicker.pick({
                            type: [DocumentPicker.types.allFiles],
                        }).then((res) => {
                            this.props.addPicture(this.state.id, res, (err, resp, callback) => {callback()})
                        }).catch((err) => {
                            if (DocumentPicker.isCancel(err)) {
                                this.setState({step: 1, name: "", timeout: 0, id: 0})
                            }
                        })
                    })
                    break
            }
            
    }

    changeVisiablity = () => this.setState({show: !this.state.show})

    render(){
        let styles = {
            header:{
                mainContainer:{
                    backgroundColor: this.props.theme ? '#ededed' : '#121212'
                },
                overlay:{
                    backgroundColor:  this.props.theme ? '#6200eeff' : '#b28bec30',
                    flexDirection: 'row',
                    width: screenWidth,
                },
                container:{
                    backgroundColor:  this.props.theme ? '#ededed' : '#121212', //'#b28bec',
                },
                title:{
                    flex: 10,
                    color: 'white',
                    fontSize: 30,
                    marginLeft: 10,
                    marginBottom: 5,
                    paddingVertical: 10,
                },
                button:{
                    container:{
                        marginLeft: 10,
                        flex: 1, 
                        justifyContent: 'center', 
                        alignItems: 'center', 
                    },
                    context:{
                        fontSize: 30,
                        fontFamily: "Material-Design-Iconic-Font",
                        color: 'white',
                    },
                },
                add:{
                    container:{
                        flex: 1, 
                        justifyContent: 'center', 
                        alignItems: 'center', 
                    },
                    context:{
                        fontSize: 35,
                        fontFamily: "Material-Design-Iconic-Font",
                        color: 'white',
                    },
                },
            },
            add:{
                name:{
                    container:{
        
                    },
                    input:{
                        borderColor:this.props.theme ? '#bb86fc' : '#00dbc5', 
                        borderWidth: 0, 
                        borderBottomWidth:2, 
                        color:'#aaaaaa', 
                        width: screenWidth - 20,
                        textAlign: 'center',
                        marginLeft: 10, 
                        marginRight: 10, 
                        marginBottom: 10, 
                        marginTop: 10,
                    },
                },
                timeout:{
                    container:{
        
                    },
                    input:{
                        borderColor:this.props.theme ? '#bb86fc' : '#00dbc5', 
                        borderWidth: 0, 
                        borderBottomWidth:2, 
                        color:'#aaaaaa', 
                        width: screenWidth - 20,
                        textAlign: 'center',
                        marginLeft: 10, 
                        marginRight: 10, 
                        marginBottom: 10, 
                        marginTop: 10,
                    },
                },
            },
            news:{
                container:{},
                elements:{
                    container:{
                        width: screenWidth - 20, 
                        marginVertical: 5, 
                        marginHorizental: 10, 
                        borderRadius: 10, 
                        backgroundColor: this.props.theme ? '#ededed' : '#121212',
                    },
                    overlay:{
                        position: 'absolute',
                        bottom: 10,
                        left: 10,
                        width: screenWidth - 20,
                        borderRadius: 10, 
                        backgroundColor: '#000000a0',//'#b28bec80',
                    },
                    context:{
                        textAlign: 'center', 
                        backgroundColor:'#55555500', 
                        color:'#ffffff', 
                        marginTop: 10,
                    },
                    timeoutAndDelete:{
                        container:{
                            height: 70,
                        },
                        delete:{
                            container:{
                                position: 'absolute', 
                                top: 0, 
                                right: 0,
                            },
                            button:{
                                container:{
                                    flex: 1, 
                                    justifyContent: 'center', 
                                    alignItems: 'center', 
                                    backgroundColor: '#00000000', 
                                    margin: 10,
                                },
                                touchableArea:{
                                    marginVertical: 2, 
                                    backgroundColor: this.props.theme ? '#bb86fc' : '#00dbc5', /* #2196F3 */ 
                                    borderRadius: 10,
                                },
                                context:{
                                    fontSize: 30, 
                                    fontFamily: "Material-Design-Iconic-Font", 
                                    padding: 10, 
                                    color: 'black',
                                },
                            },
                        },
                        timeout:{
                            duration:{
                                container:{
                                    position: 'absolute', 
                                    top: 0, 
                                    left: 5,
                                },
                                textArea:{
                                    flex: 1, 
                                    justifyContent: 'center', 
                                    alignItems: 'center', 
                                    backgroundColor: '#00000000', 
                                    margin: 10,
                                },
                                context:{
                                    fontSize: 30, 
                                    fontFamily: "Material-Design-Iconic-Font", 
                                    padding: 10, 
                                    color: 'white',
                                },
                            },
                            sign:{
                                container:{
                                    position: 'absolute', 
                                    top: 0, 
                                    left: 35,
                                },
                                textArea:{
                                    flex: 1, 
                                    justifyContent: 'center', 
                                    alignItems: 'center', 
                                    backgroundColor: '#00000000', 
                                    margin: 10,
                                },
                                context:{
                                    color: 'white',
                                },
                            },
                        },
                    },
                    image:{
                        container:{
                            flex: 1,
                            width: screenWidth,
                            height: screenHeight,
                        },
                        image:{
                            backgroundColor: this.props.theme ? '#e5e5e5' : '#000000',
                            flex: 1,
                            width: screenWidth,
                            height: screenHeight,
                            resizeMode: 'contain',
                        }
                    },
                },
            }
        }
        return(
        <View style={styles.mainContainer}>
            {this.state.show ? <View style={styles.header.container}>
                <View style={styles.header.overlay}>
                    <View style={styles.header.button.container}>
                        <TouchableNativeFeedback
                            onPress={this.sideMenu}
                            background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
                            <View>
                                <Text style={styles.header.button.context}>{"\uf197"}</Text>
                            </View>
                        </TouchableNativeFeedback>
                    </View>
                    <Text style={styles.header.title}>News</Text>
                    {this.props.online ? <View style={styles.header.add.container}>
                        <TouchableNativeFeedback
                            onPress={this.nextStep}
                            background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
                            <View>
                                <Text style={styles.header.add.context}>{this.state.step === 1 ? '\uf278' : '\uf2ee'}</Text>
                            </View>
                        </TouchableNativeFeedback>
                    </View> : null}
                </View>
            </View> : null}

            {this.state.step === 1 ? <FlatList horizontal pagingEnabled data={this.props.news} extraData={this.props} keyExtractor={(item) => item.id.toString()} renderItem={({item}) => {
                return (<View >
                    <TouchableWithoutFeedback onPress={this.changeVisiablity} style={styles.news.elements.image.container}>
                        <Image style={styles.news.elements.image.image} source={{uri: item.picture}} />
                    </TouchableWithoutFeedback>
                    {this.state.show ? (<View on style={styles.news.elements.overlay}>
                        <Text style={styles.news.elements.context}>{item.caption}</Text>
                        <View style={styles.news.elements.timeoutAndDelete.container}>
                            <View style={styles.news.elements.timeoutAndDelete.delete.container}>
                                <View style={styles.news.elements.timeoutAndDelete.delete.button.container}>
                                    {this.props.online ? <TouchableNativeFeedback
                                        onPress={() => {Alert.alert(`Delete News`,
                                            `Do You Realy Want To Delete News ${item.caption}`,[
                                                {text: "No", style:"cancel"},
                                                {text: "Yes", onPress: () => {this.props.onDeleteNews(item.id)}}
                                            ])}}
                                        style={{marginRight: 10}}
                                        background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
                                        <View style={styles.news.elements.timeoutAndDelete.delete.button.touchableArea}>
                                            <Text style={styles.news.elements.timeoutAndDelete.delete.button.context}>{"\uf154"}</Text>
                                        </View>
                                    </TouchableNativeFeedback> : null}
                                </View>
                            </View>
                            <View style={styles.news.elements.timeoutAndDelete.timeout.duration.container}>
                                <View style={styles.news.elements.timeoutAndDelete.timeout.duration.textArea}>
                                    <Text style={styles.news.elements.timeoutAndDelete.timeout.duration.context}>{"\uf334"}</Text>
                                </View>
                            </View>
                            <View style={styles.news.elements.timeoutAndDelete.timeout.sign.container}>
                                <View style={styles.news.elements.timeoutAndDelete.timeout.sign.textArea}>
                                    <Text style={styles.news.elements.timeoutAndDelete.timeout.sign.context}>{item.timeout}</Text>
                                </View>
                            </View>
                        </View>
                    </View>) : null }
                </View>)
            }} /> : null }

            {this.state.step === 2 ? <ScrollView style={styles.add.name.container}>
                <TextInput onChangeText={(newName)=>this.setState({name: newName})} placeholderTextColor={styles.add.name.input.color} placeholder={"Enter Context of New News"} style={styles.add.name.input}/>
            </ScrollView> : null}

            {this.state.step === 3 ? <ScrollView style={styles.add.timeout.container}>
                <TextInput onChangeText={(newTimeout)=>this.setState({timeout: newTimeout})} placeholderTextColor={styles.add.timeout.input.color} placeholder={"Enter Timeout of New News"} keyboardType={"numeric"} style={styles.add.timeout.input}/>
            </ScrollView> : null}

            {this.state.sideMenu === true ?(<SideMenu online={this.props.online} orientation={this.props.orientation} screenWidth={screenWidth} screenHeight={screenHeight} sideMenuContent={this.props.sideMenuContent} theme={this.props.theme} />):null}
        </View>
        )
    }
}

