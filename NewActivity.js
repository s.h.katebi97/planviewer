import React, {Component} from 'react'
import {ScrollView, View, Text, TextInput, Picker, TouchableNativeFeedback, Dimensions, Platform} from 'react-native'
import DatePicker from 'react-native-datepicker'
import { SelectDays } from './SelectDays.js'

let screenHeight = Dimensions.get('window').height
let screenWidth = Dimensions.get('window').width

let stages = {
    nameAndDetails: 1,

}

export class NewActivity extends Component{
    constructor(props){
        super(props)
        this.state={
            name: "",
            detail: "",
            selectedPeriod: 'N',
            selectedType: 1,
            selectedPriority: 1,
            startDate: new Date(),
            endDate: new Date(),
            startTime: new Date(),
            endTime: new Date(),
            selectedDays: [],
            disablebutton: false,
        }
    }

    render(){
        let dailyDays = [{name: 'Sat', number: 1},
        {name: 'Sun', number: 2},
        {name: 'Mon', number: 3},
        {name: 'Thu', number: 4},
        {name: 'Wen', number: 5},
        {name: 'Thr', number: 6},
        {name: 'Fri', number: 7},]
        let monthlyDays = []
        for(let i = 1 ; i <= 31 ; i++) monthlyDays.push({name: i, number: i})
        let annualDays = []
        for(let i = 1 ; i <= 366 ; i++) annualDays.push({name: i, number: i})
        let styles = {
            guideTextStyle: {
                backgroundColor:'#FFFFFF00',
                textAlign: 'center',
                color: this.props.theme ? '#121212' : '#aaaaaa',
                width: screenWidth - 20,
                marginLeft: 10, 
                marginRight: 10, 
                marginBottom: 10, 
                marginTop: 10,
            },
            containerStyle:{
                flex: 1,
                backgroundColor: this.props.theme ? '#e5e5e5' : '#000000',
                width: screenWidth,
            },
            scrollAreaStyle:{
                flex: 1,
                backgroundColor: this.props.theme ? '#e5e5e5' : '#000000',
                width: screenWidth,
            },
            inputStyle:{
                borderColor:this.props.theme ? '#bb86fc' : '#00dbc5', 
                borderWidth: 0, 
                borderBottomWidth:2, 
                color:this.props.theme ? '#121212' : '#aaaaaa', 
                width: screenWidth - 20,
                textAlign: 'center',
                marginLeft: 10, 
                marginRight: 10, 
                marginBottom: 10, 
                marginTop: 10,
            },
            datePikerStyle:{
                borderColor:this.props.theme ? '#bb86fc' : '#00dbc5', 
                borderWidth: 0, 
                borderBottomWidth:2, 
                width: screenWidth - 20,
                marginLeft: 10, 
                marginRight: 10, 
                marginBottom: 10, 
                marginTop: 10,
            },
            pickerStyle:{
                backgroundColor: this.props.theme ? '#ffffff' : '#121212',
                borderColor:this.props.theme ? '#bb86fc' : '#00dbc5', 
                borderWidth: 0, 
                borderBottomWidth:2, 
                color:this.props.theme ? '#bb86fc' : '#00dbc5', 
                width: screenWidth - 20,
                textAlign: 'center',
                marginLeft: 10, 
                marginRight: 10, 
                marginBottom: 10, 
                marginTop: 10,
                elevation: 3,
            },
            buttonStyle:{
                height: 50,
                width: 50,
                backgroundColor: this.props.theme ? '#bb86fc' : '#00dbc5', /* #2196F3 */ 
                borderRadius: 25,
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
            },
            buttonContainerStyle: {
                position: 'absolute',
                right: 20,
                bottom: 20,
                justifyContent: 'center',
            },
            backButtonContainerStyle: {
                position: 'absolute',
                left: 20,
                bottom: 20,
                justifyContent: 'center',
            },
            context:{
                fontSize: 20, 
                fontFamily: "Material-Design-Iconic-Font", 
                color: 'black',
            },
        }
        return(
            <View style={styles.containerStyle}>
                <ScrollView style={styles.scrollAreaStyle}>
                    <TextInput onChangeText={(newName)=>this.setState({name: newName})} style={styles.inputStyle} placeholderTextColor={styles.inputStyle.color} placeholder={"Activity Name"}/>
                    <TextInput onChangeText={(newDetail)=>this.setState({detail: newDetail})} style={styles.inputStyle} placeholderTextColor={styles.inputStyle.color} placeholder={"Activity Detail"}/>
                    <Picker style={styles.pickerStyle} selectedValue={this.state.selectedPeriod} onValueChange={(newPreiod)=>this.setState({selectedPeriod: newPreiod})}>
                        <Picker.Item style={styles.inputStyle} label="Non-Periodic" value="N" />
                        <Picker.Item style={styles.inputStyle} label="Daily" value="D" />
                        <Picker.Item style={styles.inputStyle} label="Weekly" value="W" />
                        <Picker.Item style={styles.inputStyle} label="Monthly" value="M" />
                        <Picker.Item style={styles.inputStyle} label="Annual" value="A" />
                    </Picker>

                    {['W','M','A'].includes(this.state.selectedPeriod) ? <SelectDays 
                        height={screenWidth/7} 
                        width={screenWidth} 
                        foregroundColor={this.props.theme ? '#cccccc' : '#121212'} 
                        backgroundColor={this.props.theme ? '#e5e5e5' : '#000000'} 
                        clickedColor={this.props.theme ? '#bb86fc' : '#00dbc5'} 
                        textUnclickedColor={this.props.theme ? 'black' : 'white'} 
                        textClickedColor={'black'} 
                        days={this.state.selectedPeriod === 'W' ? dailyDays : this.state.selectedPeriod === 'M' ? monthlyDays : this.state.selectedPeriod === 'A' ? annualDays : []} 
                        onUpdate={(days) => {this.setState({selectedDays: days})}} 
                    /> : null}

                    <View style={{flexDirection: 'row'}}>
                        <Text style={{...styles.guideTextStyle, width: styles.guideTextStyle.width/2 - 10, marginRight: 5}}>Start Date</Text>
                        <Text style={{...styles.guideTextStyle, width: styles.guideTextStyle.width/2 - 10, marginLeft: 5}}>End Date</Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <DatePicker customStyles={{dateText:{color: styles.inputStyle.color}}} date={this.state.startDate} style={{...styles.datePikerStyle, width: styles.datePikerStyle.width/2 - 10, marginRight: 5, borderBottomWidth: 0}} onDateChange={(date) => this.setState({startDate: date})}/>
                        <DatePicker customStyles={{dateText:{color: styles.inputStyle.color}}} date={this.state.endDate} style={{...styles.datePikerStyle, width: styles.datePikerStyle.width/2 - 10, marginLeft: 5, borderBottomWidth: 0}} onDateChange={(date) => this.setState({endDate: date})}/>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <Text style={{...styles.guideTextStyle, width: styles.guideTextStyle.width/2 - 10, marginRight: 5}}>Start Time</Text>
                        <Text style={{...styles.guideTextStyle, width: styles.guideTextStyle.width/2 - 10, marginLeft: 5}}>End Time</Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <DatePicker customStyles={{dateText:{color: styles.inputStyle.color}}} date={this.state.startTime} mode='time' style={{...styles.datePikerStyle, width: styles.datePikerStyle.width/2 - 10, marginRight: 5, borderBottomWidth: 0}} onDateChange={(date) => this.setState({startTime: date})}/>
                        <DatePicker customStyles={{dateText:{color: styles.inputStyle.color}}} date={this.state.endTime} mode='time' style={{...styles.datePikerStyle, width: styles.datePikerStyle.width/2 - 10, marginLeft: 5, borderBottomWidth: 0}} onDateChange={(date) => this.setState({endTime: date})}/>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <Text style={styles.guideTextStyle}>Activity Type</Text>
                    </View>
                    <Picker style={styles.pickerStyle} selectedValue={this.state.selectedType} onValueChange={(newType)=>this.setState({selectedType: newType})}>
                        {this.props.activityTypes.map((item) => <Picker.Item label={item.name} value={item.id} />)}
                    </Picker>
                    <View style={{flexDirection: 'row'}}>
                        <Text style={styles.guideTextStyle}>Activity Priority</Text>
                    </View>
                    <Picker style={styles.pickerStyle} selectedValue={this.state.selectedPriority} onValueChange={(newPriority)=>this.setState({selectedPriority: newPriority})}>
                        {this.props.activityPriorities.map((item) => <Picker.Item label={item.name} value={item.id} />)}
                    </Picker>
                </ScrollView>
                <View style={styles.buttonContainerStyle}>
                    <TouchableNativeFeedback
                        disabled={this.state.disableButton}
                        onPress={() => this.setState({disableButton: true}, this.props.onFinish(this.state.name, this.state.detail, this.state.selectedType, this.state.selectedPriority, this.state.selectedPeriod, this.state.startTime, this.state.endTime, this.state.startDate, this.state.endDate, this.state.selectedDays))}
                        background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
                        <View style={styles.buttonStyle}>
                            <Text style={styles.context}>{"\uf2ee"}</Text>
                        </View>
                    </TouchableNativeFeedback>
                </View>
                <View style={styles.backButtonContainerStyle}>
                    <TouchableNativeFeedback
                        disabled={this.state.disableButton}
                        onPress={() => this.setState({disableButton: true}, this.props.onBack())}
                        background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
                        <View style={styles.buttonStyle}>
                            <Text style={styles.context}>{"\uf118"}</Text>
                        </View>
                    </TouchableNativeFeedback>
                </View>
            </View>
        )
    }
}
