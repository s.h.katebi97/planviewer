import React, { Component } from 'react'
import { View, TextInput, Dimensions } from 'react-native'

let screenHeight = Dimensions.get('window').height
let screenWidth = Dimensions.get('window').width

export class Register
 extends Component{
    constructor(props){
        super(props)
        this.state={

        }
    } 

    render(){
        let styles = {
            inputStyle:{
                borderColor:this.props.theme ? '#bb86fc' : '#00dbc5', 
                borderWidth: 0, 
                borderBottomWidth:2, 
                color:this.props.theme ? '#121212' : '#aaaaaa', 
                textAlign: 'center',
                margin: 10, 
            },
            container:{
                paddingHorizontal: 10,
                justifyContent: 'space-around',
                flex: 1,
                backgroundColor: this.props.theme ? '#ffffff' : '#121212',
                width: screenWidth - 20,
                height: screenHeight - 300,
                borderRadius: 10,
            },
            name:{
                prefix:{
                    flex: 1.2,
                    borderColor:this.props.theme ? '#bb86fc' : '#00dbc5', 
                    backgroundColor:'#FFFFFF00',
                    borderWidth: 0, 
                    borderBottomWidth:2, 
                    color:this.props.theme ? '#121212' : '#aaaaaa', 
                    textAlign: 'center',
                    margin: 10, 
                },
                first:{
                    flex: 4,
                    borderColor:this.props.theme ? '#bb86fc' : '#00dbc5', 
                    backgroundColor:'#FFFFFF00',
                    borderWidth: 0, 
                    borderBottomWidth:2, 
                    color:this.props.theme ? '#121212' : '#aaaaaa', 
                    textAlign: 'center',
                    margin: 10, 
                },
                middle:{
                    borderColor:this.props.theme ? '#bb86fc' : '#00dbc5', 
                    backgroundColor:'#FFFFFF00',
                    borderWidth: 0,
                    borderBottomWidth:2, 
                    color:this.props.theme ? '#121212' : '#aaaaaa', 
                    textAlign: 'center',
                    margin: 10, 
                    marginHorizontal: 10,
                },
                last:{
                    flex: 4,
                    borderColor:this.props.theme ? '#bb86fc' : '#00dbc5', 
                    backgroundColor:'#FFFFFF00',
                    borderWidth: 0, 
                    borderBottomWidth:2, 
                    color:this.props.theme ? '#121212' : '#aaaaaa', 
                    textAlign: 'center',
                    margin: 10, 
                },
            },
            account:{
                email:{
                    borderColor:this.props.theme ? '#bb86fc' : '#00dbc5', 
                    borderWidth: 0, 
                    borderBottomWidth:2, 
                    color:this.props.theme ? '#121212' : '#aaaaaa', 
                    textAlign: 'center',
                    margin: 10, 
                    marginBottom: 40,
                },
                password:{
                    borderColor:this.props.theme ? '#bb86fc' : '#00dbc5', 
                    borderWidth: 0, 
                    borderBottomWidth:2, 
                    color:this.props.theme ? '#121212' : '#aaaaaa', 
                    textAlign: 'center',
                    margin: 10, 
                },
                username:{
                    borderColor:this.props.theme ? '#bb86fc' : '#00dbc5', 
                    borderWidth: 0, 
                    borderBottomWidth:2, 
                    color:this.props.theme ? '#121212' : '#aaaaaa', 
                    textAlign: 'center',
                    margin: 10, 
                },
            },
        }
        return(
                <View style={styles.container}>
                    <View style={{flexDirection: 'row'}}>
                        <TextInput style={styles.name.prefix} placeholderTextColor={styles.name.prefix.color} placeholder="Prefix" onChangeText={this.props.updatePrefix} value={this.props.prefix} blurOnSubmit/>
                        <TextInput style={styles.name.first} placeholderTextColor={styles.name.first.color} placeholder="First Name" onChangeText={this.props.updateFirst} value={this.props.first} blurOnSubmit/>
                        <TextInput style={styles.name.last} placeholderTextColor={styles.name.last.color} placeholder="Last Name" onChangeText={this.props.updateLast} value={this.props.last} blurOnSubmit/>
                    </View>
                    <TextInput style={styles.name.middle} placeholderTextColor={styles.name.middle.color} placeholder="Middle Name" onChangeText={this.props.updateMiddle} value={this.props.middle} blurOnSubmit/>
                    <TextInput style={styles.account.username} placeholderTextColor={styles.inputStyle.color} placeholder="Username" onChangeText={this.props.updateUsername} value={this.props.username} blurOnSubmit/>
                    <TextInput style={styles.account.password} placeholderTextColor={styles.inputStyle.color} placeholder="Password" onChangeText={this.props.updatePassword} value={this.props.password} blurOnSubmit/>
                    <TextInput style={styles.account.email} placeholderTextColor={styles.inputStyle.color} placeholder="E-Mail" onChangeText={this.props.updateEmail} value={this.props.email} blurOnSubmit/>
                </View>
        )
    }
}


