import React, { Component } from 'react';
import { Picker, ScrollView, Text, View, TouchableNativeFeedback, Platform, Dimensions } from 'react-native';
import { CheckBox } from 'react-native-elements'

let screenHeight = Dimensions.get('window').height
let screenWidth = Dimensions.get('window').width

export class NewAccess extends Component{
    constructor(props){
        super(props)
        this.state={
            selectedPlan: props.plans[0].id,
            selectedType: 0,
            selectedPriority: 0,
            showDetail: false,
            showBrief: false,
            disableButton: false,
        }
    }

    changePermisionOfActivities = () => {
        let selectedActivities = this.props.activities.filter(function (activity) {
            return (this.type === 0 || activity.type.id === this.type) && (this.priority === 0 || activity.priority.id === this.priority)
        }, {type: this.state.selectedType, priority: this.state.selectedPriority})
        for (activity of selectedActivities){
            console.warn(activity)
            if(!this.props.onFinish(this.state.selectedPlan, activity.id, this.state.showDetail, this.state.showBrief))
                return
        }
        this.props.onSuccess()
    }

    render(){
        let styles = {
            guideTextStyle: {
                backgroundColor:'#FFFFFF00',
                textAlign: 'center',
                color: this.props.theme ? '#121212' : '#aaaaaa',
                width: screenWidth - 20,
                marginLeft: 10, 
                marginRight: 10, 
                marginBottom: 10, 
                marginTop: 10,
            },
            containerStyle:{
                flex: 1,
                backgroundColor: this.props.theme ? '#e5e5e5' : '#000000',
                width: screenWidth,
            },
            scrollAreaStyle:{
                flex: 1,
                backgroundColor: this.props.theme ? '#e5e5e5' : '#000000',
                width: screenWidth,
            },
            inputStyle:{
                backgroundColor: this.props.theme ? '#ffffff' : '#121212', 
                color:this.props.theme ? '#bb86fc' : '#00dbc5', 
                width: screenWidth - 20,
                marginLeft: 10, 
                marginRight: 10, 
                marginBottom: 10, 
                marginTop: 10,
                borderColor: this.props.theme ? '#bb86fc' : '#00dbc5', 
                borderWidth: 0, 
                borderBottomWidth: 2,
                elevation: 3,
            },
            buttonStyle:{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                margin: 20,
            },
            button:{
                height: 50,
                width: 50,
                justifyContent: 'center',
                alignItems: 'center',
            },
            backButtonContainerStyle: {
                position: 'absolute',
                left: 10,
                bottom: 10,
                flex: 0,
                justifyContent: 'center',
            },
            buttonContainerStyle: {
                position: 'absolute',
                right: 20,
                bottom: 20,
                justifyContent: 'center',
            },
            backButtonContainerStyle: {
                position: 'absolute',
                left: 20,
                bottom: 20,
                justifyContent: 'center',
            },
            buttonStyle:{
                height: 50,
                width: 50,
                backgroundColor: this.props.theme ? '#bb86fc' : '#00dbc5', /* #2196F3 */ 
                borderRadius: 25,
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
            },
            buttonText: {
                fontSize: 20, 
                fontFamily: "Material-Design-Iconic-Font", 
                color: 'black',
            },
        }
        return(
            <View style={styles.containerStyle}>
                <ScrollView style={styles.scrollAreaStyle}>
                    <Text style={styles.guideTextStyle}>Plan</Text>
                    <Picker style={styles.inputStyle} selectedValue={this.state.selectedPlan} onValueChange={(newPlan)=>this.setState({selectedPlan: newPlan})}>
                        {this.props.plans.map((item) => <Picker.Item label={item.name} value={item.id} />)}
                    </Picker>

                    <Text style={styles.guideTextStyle}>Grant Following Accesses To Actvity Type</Text>
                    <Picker style={styles.inputStyle} selectedValue={this.state.selectedType} onValueChange={(newType)=>this.setState({selectedType: newType})}>
                        {this.props.activityTypes.map((item) => <Picker.Item label={item.name} value={item.id} />)}
                        <Picker.Item  label={"All"} value={0} />
                    </Picker>
                    
                    <Text style={styles.guideTextStyle}>Grant Following Accesses To Actvity Priority</Text>
                    <Picker style={styles.inputStyle} selectedValue={this.state.selectedPriority} onValueChange={(newPriority)=>this.setState({selectedPriority: newPriority})}>
                        {this.props.activityPriorities.map((item) => <Picker.Item label={item.name} value={item.id} />)}
                        <Picker.Item label={"All"} value={0} />
                    </Picker>

                    <View style={{flexDirection: 'row'}}>
                        <View style={{flexDirection: 'row', width: styles.guideTextStyle.width/2}}>
                            <CheckBox containerStyle={{backgroundColor: '#00000000', borderWidth:0}} title={"Show Details"} checked={this.state.showDetail} onPress={() => this.setState({ showDetail: !this.state.showDetail })} />
                        </View>
                        <View style={{flexDirection: 'row', width: styles.guideTextStyle.width/2}}>
                            <CheckBox containerStyle={{backgroundColor: '#00000000', borderWidth:0}} title={"Show Brief"} checked={this.state.showBrief} onPress={() => this.setState({ showBrief: !this.state.showBrief })} />
                        </View>
                    </View>
                </ScrollView>
                <View style={styles.buttonContainerStyle}>
                    <View style={styles.buttonStyle}>
                        <TouchableNativeFeedback
                            disabled={this.state.disableButton}
                            onPress={() => this.setState({disableButton: true}, this.changePermisionOfActivities())}
                            background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
                        <View style={styles.button}>
                            <Text style={styles.buttonText}>{"\uf2ee"}</Text>
                        </View>
                        </TouchableNativeFeedback>
                    </View>
                </View>
                <View style={styles.backButtonContainerStyle}>
                    <View style={styles.buttonStyle}>
                        <TouchableNativeFeedback
                            disabled={this.state.disableButton}
                            onPress={() => this.setState({disableButton: true}, this.props.onBack())}
                            background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
                            <View style={styles.button}>
                                <Text style={styles.buttonText}>{"\uf118"}</Text>
                            </View>
                        </TouchableNativeFeedback>
                    </View>
                </View>
            </View>
        )
    }
}
